/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/***/ (() => {

//page acceuil a remettre
// logoAnimation = window.onload = function () {
// 		setTimeout(func1, 3000);
// }
var counter = 0;

function func1() {
  document.getElementById("divContent").className = "show";
  document.getElementById("divLogo").style.display = "none";
}

formulaireChange = window.onload = function () {
  var moreFields = function moreFields() {
    if (counter < 3) {
      counter++;
      var newFields = document.getElementById('readroot').cloneNode(true);
      newFields.id = '';
      newFields.style.display = 'block';
      var newField = newFields.childNodes;

      for (var i = 0; i < newField.length; i++) {
        var theName = newField[5].children[i].children[0].children[1].name;

        if (theName) {
          newField[5].children[i].children[0].children[1].name = theName + "[]";
        }
      }

      var insertHere = document.getElementById('writeroot');
      insertHere.parentNode.insertBefore(newFields, insertHere);
    } else if (counter == 3) {
      counter++;
      document.getElementById("finEss").innerHTML = "\n\t\t\t<input class='mt-3' type=\"button\" value=\"Supprimer l'essence\" onclick=\"DeleteInner();CompteurSubtract()\" /><br /><br />\n\t\t\t<div class='d-flex row'>\n\t\t\t\t<div class='col-2'>\n\t\t\t\t\t<div class='form-group'>\n\t\t\t\t\t\t<label for='exampleInput1'>Liste ESS</label>\n\t\t\t\t\t\t<input name='listeESS[]' type='' class='form-control' name='listeESS' id='ListeEss' aria-describedby='Help' placeholder='Enter message'>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class='col-2'>\n\t\t\t\t\t<div class='form-group'>\n\t\t\t\t\t\t<label for=''>Plot/arriv\xE9s</label>\n\t\t\t\t\t\t<select name='plotArrive[]' id=\"plotArrive\" name='plotArrive' onchange=\"\">\n\t\t\t\t\t\t\t<option value=\"Plot\">Plot</option>\n\t\t\t\t\t\t\t<option value=\"Arriv\xE9s\">Arriv\xE9s</option>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class='col-2'>\n\t\t\t\t\t<div class='form-group'>\n\t\t\t\t\t\t<label for='exampleInput1'>Ep max (mm)</label>\n\t\t\t\t\t\t<input name='epMax[]' type='' class='form-control' id='epMax' name='epMax4' aria-describedby='Help' placeholder='Enter message'>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class='col-2'>\n\t\t\t\t\t<div class='form-group'>\n\t\t\t\t\t\t<label for='exampleInput1'>Longueur max (m)</label>\n\t\t\t\t\t\t<input name='longueurMax[]' type='' class='form-control' name='longueurMax4' id='longueurMax' aria-describedby='Help' placeholder='Enter message'>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class='col-2'>\n\t\t\t\t\t<div class='form-group'>\n\t\t\t\t\t\t<label for=''>% Humidit\xE9 d\xE9part</label>\n\t\t\t\t\t\t<select id=\"humiditeDepart\" name='humiditeDepart[]' onchange=\"\">\n\t\t\t\t\t\t\t<option value=\"Valeur\">Valeur</option>\n\t\t\t\t\t\t\t<option value=\"R\xE9ssuy\xE9\">R\xE9ssuy\xE9</option>\n\t\t\t\t\t\t\t<option value=\"Vert\">Vert</option>\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class='col-2'>\n\t\t\t\t\t<div class='form-group'>\n\t\t\t\t\t\t<label for='exampleInput1'>% Humidit\xE9 finale voulue</label>\n\t\t\t\t\t\t<input type='' class='form-control' name=\"humiditeFinale[]\" id='humiditeFinale' aria-describedby='Help' placeholder='Enter message'>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class='col-12 text-center'>\n\t\t\t\t\t<div class='form-group centerform'>\n\t\t\t\t\t\t<label for='exampleInput1'>Volume mensuel \xE0 s\xE9cher </label>\n\t\t\t\t\t\t<input type='' class='form-control' name='volumeSechage[]' id='volumeSechage' aria-describedby='Help' placeholder='Enter message'>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>";
    }
  };

  var test = document.getElementById('moreFields');
  test.addEventListener('click', moreFields);
  moreFields();
  var x = document.getElementById("option").value;

  if (x == 'Cellule') {
    document.getElementById("demo").innerHTML = "\n\t\t\n\t\t<div class = 'd-flex'>\n\t\t\t<div class='col-6'>\n\t\t\t\t<label for=''>La cellule sera : </label>\n\t\t\t\t<div class='form-group'>\n\t\t\t\t\t<select id=\"mySelect\" name=\"infoCellule\">\n\t\t\t\t\t\t<option value=\"Abrit\xE9e\">Abrit\xE9e</option>\n\t\t\t\t\t\t<option value=\"En plein air\">En plein air</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class='col-6'>\n\t\t\t\t<label for=''>Methode approvisonnement</label>\n\t\t\t\t<div class='form-group'>\n\t\t\t\t\t<select id=\"mySelect2\" name='approvisionnement' onchange=\"formulaireChange2()\">\n\t\t\t   \t\t\t<option value=\"Air\">Plein air</option>\n\t\t\t\t\t\t<option value=\"Chariot\">Chariot</option>\t\t\t\t\n\t\t\t\t\t\t<option value=\"Autre\">Autre</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t\t<div class='mt-2' id=\"demo2\"></div>\n\t\t<div class='form-group mt-3 pdright'>\n\t\t<label for='exampleInputEmail1'>Veuillez caract\xE9riser les piles de bois \xE0 s\xE9cher</label>\n\t\t<div>\n\t\t\t<label for='exampleInputEmail1'>Hauteur en m</label>\n\t\t\t<input name='hauteurBois' type='' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='xx m'>\n\t\t</div>\n\t\t<div>\n\t\t\t<label for='exampleInputEmail1'>Largeur en  m</label>\n\t\t\t<input name='largeurBois' type='' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='xx m'>\n\t\t</div>\n\t\t<div>\n\t\t\t<label for='exampleInputEmail1'>Longueur en  m</label>\n\t\t\t<input name = 'longueurBois' type='' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='xx m'>\n\t\t</div>\n\t  </div>\n\n\t  \t  ";
  } else if (x == 'Unite') {
    document.getElementById("demo").innerHTML = "";
  }
};

DeleteInner = function DeleteInner() {
  document.getElementById("finEss").innerHTML = "";
};

formulaireChange2 = function formulaireChange2() {
  var y = document.getElementById("mySelect2").value;

  if (y == 'Chariot') {
    document.getElementById("demo2").innerHTML = "\n\t  <div class=' mt-3 form-group'>\n\t\t<label for='exampleInputEmail1'>Renseigner la hauteur (en m)</label>\n\t\t<input name='hauteurChariot' type='' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='xx m'>\n\t  </div>\t  \n  ";
  } else if (y == 'Autre') {
    document.getElementById("demo2").innerHTML = "\n\t<div class='mt-3 form-group'>\n\t\t<label for='exampleInputEmail1'>Veuillez pr\xE9ciser</label>\n\t\t<input name='autre' type='' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='Enter message'>\n\t </div>\n  ";
  } else if (y == 'Air') {
    document.getElementById("demo2").innerHTML = "\n  ";
  }
};

resetProduit1 = function resetProduit1() {
  document.getElementById("produit1").innerHTML = "";
};

resetProduit2 = function resetProduit2() {
  document.getElementById("produit2").innerHTML = "";
};

resetProduit3 = function resetProduit3() {
  document.getElementById("produit3").innerHTML = "";
};

resetProduit1bis = function resetProduit1bis() {
  document.getElementById("produit1bis").innerHTML = "";
};

resetProduit2bis = function resetProduit2bis() {
  document.getElementById("produit2bis").innerHTML = "";
};

resetProduit3bis = function resetProduit3bis() {
  document.getElementById("produit3bis").innerHTML = "";
};

savoirPlus1 = function savoirPlus1() {
  document.getElementById("produit1").innerHTML = "\n\t<div class='d-flex row pt-4'>\n\t<div class='col-10'>\n\t\t<p class='text-white  ms-3 h2'><strong><u>La cellule</u></strong>\n\t\t</p>\n\t </div>\n\t <div class='col-2'>\n\t\t <button class='h1 text-white buttonShowHide text-danger' onclick='resetProduit1()'>-\t</button>\n\t </div>\n </div>\n\t<div class='col-12'>\n\t\t<div class='d-flex row'>\n\t\t\t<div class='col-12 col-lg-12'>\n\t\t\t\t<div class='d-flex align-items-center pe-3 pt-3 pb-3'>\n\t\t\t\t\t<div class='d-flex row'>\n\t\t\t\t\t\t<div class='col-12 pb-2'>\n\t\t\t\t\t\t\t<p class=' pe-3 ps-3 textOnBloc text-white'>Le principe essentiel de la cellule est qu\u2019elle soit le mieux isol\xE9e possible. Elle repose sur\n\t\t\t\t\t\t\tune dalle en b\xE9ton (nous vous donnons \xE0 titre indicatif comment doit \xEAtre construite cette\n\t\t\t\t\t\t\tdalle afin d\u2019avoir un coefficient de r\xE9sistivit\xE9 thermique performant). La cellule peut \xEAtre\n\t\t\t\t\t\t\tabrit\xE9e ou en ext\xE9rieur. Nous prenons en compte pour chaque situation ces param\xE8tres\n\t\t\t\t\t\t\tpour dimensionner vos cellules afin de limiter les d\xE9perditions. Nous travaillons avec deux\n\t\t\t\t\t\t\ttechnologies aujourd\u2019hui : les panneaux sandwich type \xAB frigorifique \xBB mais aussi avec une\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\tcomposition t\xF4le et laine min\xE9rale.\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\tPour l\u2019aspect dimensionnel nous nous adaptons \xE0 vos contraintes si vous en avez.\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class='d-flex row'>\n\t\t\t\t\t\t\t<div class='col-12 col-lg-6'>\n\t\t\t\t\t\t\t\t   <div class='d-flex justify-content-center'>\n\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary btn-lg pb-2 \">Nous contacter</button>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class='col-12 col-lg-6'>\n\t\t\t\t\t\t\t\t<div class='d-flex justify-content-center'>\n\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary btn-lg pb-3 \">Cahier des charges</button>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t</div>\n\t</div>\n\t";
};

savoirPlus1bis = function savoirPlus1bis() {
  document.getElementById("produit1bis").innerHTML = "\n\t<div class='d-flex row pt-4'>\n\t<div class='col-10'>\n\t<p class='text-white  ms-3 h2'><strong><u>La cellule</u></strong>\n\t</p>\n\t </div>\n\t <div class='col-2'>\n\t\t <button class='h1 text-center text-white buttonShowHide text-danger' onclick='resetProduit1bis()'>-\t</button>\n\t </div>\n </div>\n <div class='col-12'>\n <div class='d-flex row'>\n\t <div class='col-12 col-lg-12'>\n\t\t <div class='d-flex align-items-center pe-3 pt-3 pb-3'>\n\t\t\t <div class='d-flex row'>\n\t\t\t\t <div class='col-12 pb-2'>\n\t\t\t\t\t <p class=' pe-3 ps-3 textOnBloc text-white'>Le principe essentiel de la cellule est qu\u2019elle soit le mieux isol\xE9e possible. Elle repose sur\n\t\t\t\t\t une dalle en b\xE9ton (nous vous donnons \xE0 titre indicatif comment doit \xEAtre construite cette\n\t\t\t\t\t dalle afin d\u2019avoir un coefficient de r\xE9sistivit\xE9 thermique performant). La cellule peut \xEAtre\n\t\t\t\t\t abrit\xE9e ou en ext\xE9rieur. Nous prenons en compte pour chaque situation ces param\xE8tres\n\t\t\t\t\t pour dimensionner vos cellules afin de limiter les d\xE9perditions. Nous travaillons avec deux\n\t\t\t\t\t technologies aujourd\u2019hui : les panneaux sandwich type \xAB frigorifique \xBB mais aussi avec une\n\t\t\t\t\t \n\t\t\t\t\t composition t\xF4le et laine min\xE9rale.\n\t\t\t\t\t \n\t\t\t\t\t Pour l\u2019aspect dimensionnel nous nous adaptons \xE0 vos contraintes si vous en avez.\n\t\t\t\t\t </p>\n\t\t\t\t </div>\n\t\t\t\t <div class='d-flex row'>\n\t\t\t\t\t <div class='col-12 col-lg-6 pt-2'>\n\t\t\t\t\t\t\t<div class='d-flex justify-content-center'>\n\t\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-2 \">Nous contacter</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t\t <div class='col-12 col-lg-6 pt-2'>\n\t\t\t\t\t\t <div class='d-flex justify-content-center'>\n\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-3 \">Cahier des charges</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t </div>\n\t\t\t </div>\n\t\t </div>\n\t </div>\n\n </div>\n</div>\n\t";
};

savoirPlus2 = function savoirPlus2() {
  document.getElementById("produit2").innerHTML = "\n\t<div class='d-flex row pt-4'>\n\t<div class='col-10'>\n\t<p class='text-white  ms-3 h2'><strong><u>L'unit\xE9 de s\xE9chage</u></strong>\n\t</p>\n\t </div>\n\t <div class='col-2'>\n\t\t <button class='h1 text-white buttonShowHide text-danger' onclick='resetProduit2()'>-\t</button>\n\t </div>\n </div>\n <div class='col-12'>\n <div class='d-flex row'>\n\t <div class='col-12 col-lg-12'>\n\t\t <div class='d-flex align-items-center pe-3 pt-3 pb-3'>\n\t\t\t <div class='d-flex row'>\n\t\t\t\t <div class='col-12 pb-2'>\n\t\t\t\t\t <p class=' pe-3 ps-3 textOnBloc text-white'>Le principe que nous commercialisons principalement est un syst\xE8me par pompe \xE0\n\t\t\t\t\t chaleur. Le principe est simple \xE0 l\u2019inverse d\u2019un frigo domestique ce que nous cherchons \xE0\n\t\t\t\t\t faire est de produire de la chaleur. Pour ce faire, nous \xAB captons \xBB l\u2019air froid de la cellule et\n\t\t\t\t\t nous le faisons traverser un \xE9changeur. Cet air va se condenser au contact et \xE9liminer\n\t\t\t\t\t l\u2019eau contenu. Ce m\xEAme air traversera ensuite un deuxi\xE8me \xE9changeur qui r\xE9chauffera cet\n\t\t\t\t\t air pour le renvoy\xE9 dans la cellule. Nous sommes sur un circuit ferm\xE9. Les temp\xE9ratures de\n\t\t\t\t\t s\xE9chage max sont de 50 \xE0 55\xB0 C en cellule. Nous nous \xE9vertuons de travailler avec des\n\t\t\t\t\t fluides non polluant afin de pr\xE9server l\u2019environnement. Chaque unit\xE9 est sur mesure car le\n\t\t\t\t\t besoin en chaleur est d\xE9fini par les d\xE9perditions de la cellule, l\u2019humidit\xE9 de d\xE9part,\n\t\t\t\t\t width: 61%;\npadding-left: 26vw;st\xE8me est dit ENR (class\xE9 dans Energie Renouvelable), car pour 1KW \xE9lectrique fourni\n\t\t\t\t\t \n\t\t\t\t\t nous produisons 3 KW de chaleurs.\n\t\t\t\t\t \n\t\t\t\t\t Nous pouvons \xE9tudier vos autres demandes, nous avons des partenaires te prestataires de\n\t\t\t\t\t \n\t\t\t\t\t chaudi\xE8res notamment\n\t\t\t\t\t Un projet ? Des questions ?\n\t\t\t\t\t </p>\n\t\t\t\t </div>\n\t\t\t\t <div class='d-flex row'>\n\t\t\t\t\t <div class='col-12 col-lg-6'>\n\t\t\t\t\t\t\t<div class='d-flex justify-content-center'>\n\t\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-2 \">Nous contacter</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t\t <div class='col-12 col-lg-6'>\n\t\t\t\t\t\t <div class='d-flex justify-content-center'>\n\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-3 \">Cahier des charges</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t </div>\n\t\t\t </div>\n\t\t </div>\n\t </div>\n\n </div>\n</div>\n\t";
};

savoirPlus2bis = function savoirPlus2bis() {
  document.getElementById("produit2bis").innerHTML = "\n\t<div class='d-flex row pt-4'>\n\t<div class='col-10'>\n\t<p class='text-white  ms-3 h2'><strong><u>L'unit\xE9 de s\xE9chage</u></strong>\n\t</p>\n\t </div>\n\t <div class='col-2'>\n\t\t <button class='h1 text-center text-white buttonShowHide text-danger' onclick='resetProduit2bis()'>-\t</button>\n\t </div>\n </div>\n <div class='col-12'>\n <div class='d-flex row'>\n\t <div class='col-12 col-lg-12'>\n\t\t <div class='d-flex align-items-center pe-3 pt-3 pb-3'>\n\t\t\t <div class='d-flex row'>\n\t\t\t\t <div class='col-12 pb-2'>\n\t\t\t\t\t <p class=' pe-3 ps-3 textOnBloc text-white'>Le principe que nous commercialisons principalement est un syst\xE8me par pompe \xE0\n\t\t\t\t\t chaleur. Le principe est simple \xE0 l\u2019inverse d\u2019un frigo domestique ce que nous cherchons \xE0\n\t\t\t\t\t faire est de produire de la chaleur. Pour ce faire, nous \xAB captons \xBB l\u2019air froid de la cellule et\n\t\t\t\t\t nous le faisons traverser un \xE9changeur. Cet air va se condenser au contact et \xE9liminer\n\t\t\t\t\t l\u2019eau contenu. Ce m\xEAme air traversera ensuite un deuxi\xE8me \xE9changeur qui r\xE9chauffera cet\n\t\t\t\t\t air pour le renvoy\xE9 dans la cellule. Nous sommes sur un circuit ferm\xE9. Les temp\xE9ratures de\n\t\t\t\t\t s\xE9chage max sont de 50 \xE0 55\xB0 C en cellule. Nous nous \xE9vertuons de travailler avec des\n\t\t\t\t\t fluides non polluant afin de pr\xE9server l\u2019environnement. Chaque unit\xE9 est sur mesure car le\n\t\t\t\t\t besoin en chaleur est d\xE9fini par les d\xE9perditions de la cellule, l\u2019humidit\xE9 de d\xE9part,\n\t\t\t\t\t \n\t\t\t\t\t l\u2019humidit\xE9 finale, le volume et le type d\u2019essence.\n\t\t\t\t\t \n\t\t\t\t\t Ce syst\xE8me est dit ENR (class\xE9 dans Energie Renouvelable), car pour 1KW \xE9lectrique fourni\n\t\t\t\t\t \n\t\t\t\t\t nous produisons 3 KW de chaleurs.\n\t\t\t\t\t \n\t\t\t\t\t Nous pouvons \xE9tudier vos autres demandes, nous avons des partenaires te prestataires de\n\t\t\t\t\t \n\t\t\t\t\t chaudi\xE8res notamment\n\t\t\t\t\t Un projet ? Des questions ?\n\t\t\t\t\t </p>\n\t\t\t\t </div>\n\t\t\t\t <div class='d-flex row'>\n\t\t\t\t\t <div class='col-12 col-lg-6 pt-2'>\n\t\t\t\t\t\t\t<div class='d-flex justify-content-center'>\n\t\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-2 \">Nous contacter</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t\t <div class='col-12 col-lg-6 pt-2'>\n\t\t\t\t\t\t <div class='d-flex justify-content-center'>\n\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-3 \">Cahier des charges</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t </div>\n\t\t\t </div>\n\t\t </div>\n\t </div>\n\n </div>\n</div>\n\t";
};

savoirPlus3 = function savoirPlus3() {
  document.getElementById("produit3").innerHTML = "\n\t<div class='d-flex row pt-4'>\n\t<div class='col-10'>\n\t<p class='text-white  ms-3 h2'><strong><u>LA r\xE9gulation du s\xE9chage</u></strong>\n\t</p>\n\t </div>\n\t <div class='col-2'>\n\t\t <button class='h1 text-white buttonShowHide text-danger' onclick='resetProduit3()'>-\t</button>\n\t </div>\n </div>\n <div class='col-12'>\n <div class='d-flex row'>\n\t <div class='col-12 col-lg-12'>\n\t\t <div class='d-flex align-items-center pe-3 pt-3 pb-3'>\n\t\t\t <div class='d-flex row'>\n\t\t\t\t <div class='col-12 pb-2'>\n\t\t\t\t\t <p class=' pe-3 ps-3 textOnBloc text-white'>Les r\xE9gulations sont des appareils automatis\xE9s permettant au moyen de sondes de\n\t\t\t\t\t temp\xE9rature, d\u2019hygrom\xE9trie d\u2019ambiance et parfois d\u2019\xE9lectrodes de mesure pour le bois de\n\t\t\t\t\t \xAB r\xE9guler le s\xE9chage \xBB de votre bois. Ces param\xE8tres peuvent se retrouver soit en lecture\n\t\t\t\t\t directe soit en enregistrement. Nous \xE9tablissons en fonction de votre besoin le choix de\n\t\t\t\t\t l\u2019appareil qui vous correspondra. Nous d\xE9veloppons en parall\xE8le nos propres r\xE9gulations avec\n\t\t\t\t\t la conception de nos programmes en interne (permettant l\u2019intervention sur celles-ci plus\n\t\t\t\t\t \n\t\t\t\t\t rapide).\n\t\t\t\t\t \n\t\t\t\t\t Vous souhaitez faire \xE9voluer votre syst\xE8me de r\xE9gulations ? Des questions ?\n\t\t\t\t\t </p>\n\t\t\t\t </div>\n\t\t\t\t <div class='d-flex row'>\n\t\t\t\t\t <div class='col-12 col-lg-6 pt-2'>\n\t\t\t\t\t\t\t<div class='d-flex justify-content-center'>\n\t\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-2 \">Nous contacter</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t\t <div class='col-12 col-lg-6 pt-2'>\n\t\t\t\t\t\t <div class='d-flex justify-content-center'>\n\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-3 \">Cahier des charges</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t </div>\n\t\t\t </div>\n\t\t </div>\n\t </div>\n\n </div>\n</div>\n\t";
};

savoirPlus3bis = function savoirPlus3bis() {
  document.getElementById("produit3bis").innerHTML = "\n\t<div class='d-flex row pt-4'>\n\t<div class='col-10'>\n\t<p class='text-white  ms-3 h2'><strong><u>LA r\xE9gulation du s\xE9chage</u></strong>\n\t</p>\n\t </div>\n\t <div class='col-2'>\n\t\t <button class='h1 text-center text-white buttonShowHide text-danger' onclick='resetProduit3bis()'>-\t</button>\n\t </div>\n </div>\n <div class='col-12'>\n <div class='d-flex row'>\n\t <div class='col-12 col-lg-12'>\n\t\t <div class='d-flex align-items-center pe-3 pt-3 pb-3'>\n\t\t\t <div class='d-flex row'>\n\t\t\t\t <div class='col-12 pb-2'>\n\t\t\t\t\t <p class=' pe-3 ps-3 textOnBloc text-white'>Les r\xE9gulations sont des appareils automatis\xE9s permettant au moyen de sondes de\n\t\t\t\t\t temp\xE9rature, d\u2019hygrom\xE9trie d\u2019ambiance et parfois d\u2019\xE9lectrodes de mesure pour le bois de\n\t\t\t\t\t \xAB r\xE9guler le s\xE9chage \xBB de votre bois. Ces param\xE8tres peuvent se retrouver soit en lecture\n\t\t\t\t\t directe soit en enregistrement. Nous \xE9tablissons en fonction de votre besoin le choix de\n\t\t\t\t\t l\u2019appareil qui vous correspondra. Nous d\xE9veloppons en parall\xE8le nos propres r\xE9gulations avec\n\t\t\t\t\t la conception de nos programmes en interne (permettant l\u2019intervention sur celles-ci plus\n\t\t\t\t\t \n\t\t\t\t\t rapide).\n\t\t\t\t\t \n\t\t\t\t\t Vous souhaitez faire \xE9voluer votre syst\xE8me de r\xE9gulations ? Des questions ?\n\t\t\t\t\t </p>\n\t\t\t\t </div>\n\t\t\t\t <div class='d-flex row'>\n\t\t\t\t\t <div class='col-12 col-lg-6'>\n\t\t\t\t\t\t\t<div class='d-flex justify-content-center'>\n\t\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-2 \">Nous contacter</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t\t <div class='col-12 col-lg-6'>\n\t\t\t\t\t\t <div class='d-flex justify-content-center'>\n\t\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary btn-lg pb-3 \">Cahier des charges</button>\n\t\t\t\t\t\t </div>\n\t\t\t\t\t </div>\n\t\t\t\t </div>\n\t\t\t </div>\n\t\t </div>\n\t </div>\n\n </div>\n</div>\n\t";
};

CompteurSubtract = function CompteurSubtract() {
  counter--;
};

faq1 = function faq1() {
  document.getElementById("faq1").innerHTML = "\n\t<p class='pe-3 ps-3'>La mesure de l\u2019humidit\xE9 du bois est d\xE9finie comme le rapport de la masse d\u2019eau qu\u2019il contient sur sa masse anhydre (masse du bois sans eau). Au moment de l\u2019abattage, le bois peut contenir plus d\u2019eau que de mati\xE8re-bois : l\u2019humidit\xE9 est alors sup\xE9rieure \xE0 100%. Les vides cellulaires d\u2019un bois vert sont remplis d\u2019eau libre ou capillaire. Progressivement cette eau s\u2019\xE9vapore sans que le bois ne subisse de retrait et de d\xE9formation : c\u2019est la phase de \xAB ressuyage \xBB. Lorsque l\u2019eau libre a enti\xE8rement disparue, il ne reste que l\u2019eau li\xE9e qui impr\xE8gne les membranes des cellules. Elle n\xE9cessite, pour son extraction, une \xE9nergie sup\xE9rieure pour vaincre les liaisons chimiques (Van Der Waals). L\u2019\xE9vaporation de cette derni\xE8re entra\xEEne des ph\xE9nom\xE8nes de retrait et de d\xE9formation. Le point de saturation des fibres, en-dessous duquel se manifeste le \xAB jeu du bois \xBB, se situe aux alentours de 30% pour toutes les essences.</p>\n\t";
};

hideFaq1 = function hideFaq1() {
  document.getElementById("faq1").innerHTML = "";
};

faq2 = function faq2() {
  document.getElementById("faq2").innerHTML = "\n\t\n\t<p class='pe-3 ps-3'>Le retrait et le gonflement sont directement proportionnel \xE0 la diminution ou \xE0 l\u2019augmentation du taux d\u2019humidit\xE9 du bois. Chaque essence est caract\xE9ris\xE9e par trois coefficients de r\xE9tractabilit\xE9 qui expriment les variations dimensionnelles d\u2019une pi\xE8ce de bois pour une variation d\u2019humidit\xE9 de 1% selon chacune des trois directions :</p>\n\t<p class='pe-3 ps-3'>\u2022  Le retrait axial</p>\n\t<p class='pe-3 ps-3'>\u2022 Le retrait radial (d\xE9bit sur quartier)</p>\n\t<p class='pe-3 ps-3'>\u2022  Le retrait tangentiel (d\xE9bit sur dosse)</p>\n\t<p class='pe-3 ps-3'>En fonction de la temp\xE9rature et surtout de l\u2019humidit\xE9 de l\u2019air ambiant, le bois se stabilise \xE0 une humidit\xE9 d\u2019\xE9quilibre appel\xE9 \xE9quilibre hygroscopique.</p>\n\t";
};

hideFaq2 = function hideFaq2() {
  document.getElementById("faq2").innerHTML = "";
};

faq3 = function faq3() {
  document.getElementById("faq3").innerHTML = "\n\t<p class='pe-3 ps-3'>Le s\xE9chage est une \xE9tape incontournable dans la cha\xEEne de la transformation du bois. Ce processus consiste \xE0 extraire du bois une partie plus ou moins importante de son contenu en eau afin d'abaisser sa teneur en humidit\xE9 \xE0 un niveau correspondant soit \xE0 l'humidit\xE9 d'\xE9quilibre requise pour son usage final En France, pour les menuiseries ext\xE9rieures l\u2019humidit\xE9 du bois devra se situ\xE9e entre 15 et 18 % tout comme le bois utilis\xE9 pour la charpente traditionnelle. Pour le bois travaill\xE9 pour les charpentes en lamell\xE9-coll\xE9 la teneur en eau sera entre 12 et 14%. Les fermettes emploieront du bois s\xE9ch\xE9 entre 18 et 22%. Pour le secteur des menuiseries d\u2019int\xE9rieures / meubles (label NF) le taux d\u2019humidit\xE9 sera entre 10 et 12%. Pour le bois des parquets la teneur en eau entre 8 et 12%.</p>\n\t";
};

hideFaq3 = function hideFaq3() {
  document.getElementById("faq3").innerHTML = "";
};

faq4 = function faq4() {
  document.getElementById("faq4").innerHTML = "\n\t<p class='pe-3 ps-3'><strong>Le s\xE9chage naturel</strong></p>\n          <p class='pe-3 ps-3'>C\u2019est la technique la plus simple et la plus \xE9conomique. Toutefois le s\xE9chage \xE0 l\u2019air libre n\xE9cessite une zone de stockage importante et reste tr\xE8s d\xE9pendant des conditions climatiques. Immobilisant \xE0 des fins non productives des capitaux importants, il est plus le r\xE9sultat de m\xE9ventes que le produit d\u2019une volont\xE9, d\u2019autant que l\u2019humidit\xE9 finale ne satisfera aucun utilisateur ; grande dispersion des taux d\u2019humidit\xE9 finale et difficult\xE9 d\u2019atteindre des taux inf\xE9rieurs \xE0 20%. Il ne sera donc pas vendu comme bois sec.\n          Le s\xE9chage \xE0 l\u2019air libre : l\u2019alternance du jour et de la nuit permet un s\xE9chage peu brutal. En effet, la diminution de la temp\xE9rature de l\u2019air et l\u2019augmentation de son humidit\xE9 relative pendant la nuit permettent l\u2019\xE9l\xE9vation de l\u2019\xE9quilibre hygroscopique du bois et ainsi sa teneur en eau. Il en r\xE9sulte une diminution des contraintes de traction \xE0 la p\xE9riph\xE9rie du bois pendant les deux premi\xE8res phases de s\xE9chage, avant l\u2019inversion des contraintes. En outre, le s\xE9chage naturel, par sa faible temp\xE9rature d\u2019air, est plus favorable que le s\xE9chage artificiel du point de vue des discolorations apparaissant sur le ch\xEAne. N\xE9anmoins, comme nous l\u2019avons mentionn\xE9 auparavant, ce mode de s\xE9chage pr\xE9sente des risques de d\xE9gradation du bois.</p>\n          <p class='pe-3 ps-3'><strong>Les s\xE9choirs de ressuyage </strong></p>\n          <p class='pe-3 ps-3'>Les sciages verts sont ressuy\xE9s dans un vaste b\xE2timent (pr\xE9-s\xE9choir) jusqu\u2019\xE0 une humidit\xE9 voisine du point de saturation des fibres (25-30%), ensuite ils sont dirig\xE9s vers des cellules de type classique pour une finition rapide \xE0 10-12%. Les bois sont ensuite remplac\xE9s au fur et \xE0 mesure de la siccit\xE9 vis\xE9e sans que cela n\u2019affecte le ressuyage en cours des autres sciages. Ce proc\xE9d\xE9 est peu utilis\xE9 en France</p>\n          <p class='pe-3 ps-3'><strong>Les s\xE9choirs par air chaud climatis\xE9</strong></p>\n          <p class='pe-3 ps-3'>Ce proc\xE9d\xE9 est s\xFBrement le plus r\xE9pandu en France. L\u2019air circule en circuit ouvert et l\u2019abaissement de l\u2019humidit\xE9 relative de l\u2019air se r\xE9alise avec \xE9changes par l\u2019ext\xE9rieur. L\u2019air issu de la pile de bois est m\xE9lang\xE9 avec l\u2019air ext\xE9rieur, passe par une batterie de chauffe o\xF9 il est port\xE9 \xE0 la temp\xE9rature souhait\xE9e. Les temp\xE9ratures de fonctionnement sont g\xE9n\xE9ralement comprises entre 30 et 80 \xB0C. Pour certains r\xE9sineux, elles peuvent atteindre les 100\xB0C. Ces s\xE9choirs imposent l\u2019utilisation d\u2019une chaudi\xE8re.</p>\n          <p class='pe-3 ps-3'><strong>Les s\xE9choirs par pompe \xE0 chaleur</strong></p>\n          <p class='pe-3 ps-3'>Ce type de s\xE9choir se caract\xE9rise par le fait que la seule source d\u2019\xE9nergie est l\u2019\xE9lectricit\xE9. L\u2019id\xE9e a \xE9t\xE9 d\u2019adapter le principe de la pompe \xE0 chaleur au probl\xE8me du s\xE9chage du bois, c\u2019est \xE0 dire une faible consommation \xE9nerg\xE9tique : rendement \xE9nergie rendue/\xE9nergie consomm\xE9e. La pompe \xE0 chaleur est un groupe frigorifique dont l\u2019\xE9nergie \xE9lectrique est fournie pour assurer le travail m\xE9canique du compresseur et, est r\xE9cup\xE9r\xE9e avec un rendement de 2 \xE0 4 calories et frigories sur les batteries chaudes et froides, ceci expliqu\xE9 sommairement pour \xE9clairer ce terme dont on entend parler de plus en plus que ce soit en chauffage ou en climatisation\n          Dans une cellule isol\xE9e thermiquement parfaitement \xE9tanche, \xE0 l\u2019aide de ventilateurs, de l\u2019air chauff\xE9 est souffl\xE9 et brass\xE9 \xE0 travers les paquets de bois o\xF9 il se charge en humidit\xE9 ; cet air est ensuite absorb\xE9 par l\u2019appareil, ass\xE9ch\xE9 (\xE9vaporateur ou batterie froide) il abandonne alors l\u2019eau \xE9vapor\xE9e du bois, puis il est chauff\xE9 (condenseur ou batterie chaude) et renvoy\xE9 dans la cellule pour un nouveau cycle. La batterie froide condense l\u2019humidit\xE9 du bois transform\xE9e en liquide. La batterie chaude fait partie du circuit interne de la pompe \xE0 chaleur, compos\xE9e de l\u2019\xE9vaporateur, du compresseur et du condenseur. L\u2019\xE9vaporateur fixe l\u2019humidit\xE9, le compresseur v\xE9hicule l\u2019\xE9nergie et le condenseur \xE9vacue la chaleur contenue dans les vapeurs refoul\xE9es par le compresseur. Afin d\u2019acc\xE9l\xE9rer la mont\xE9e en temp\xE9rature du s\xE9choir, des r\xE9sistances \xE9lectriques sont mises en \u0153uvre au d\xE9but de chaque cycle.\n          Le s\xE9chage par pompe \xE0 chaleur : moins de risque de d\xE9gradations du bois (type collapse, discoloration, etc.) en raison de l\u2019utilisation de temp\xE9ratures faibles. Cependant, ce niveau faible en temp\xE9rature ne permet pas une activation de la visco\xE9lasticit\xE9, caract\xE9ristique favorable \xE0 la qualit\xE9 du mat\xE9riau de point de vue des contraintes de s\xE9chage.</p>\n          <p class='pe-3 ps-3'><strong>Les s\xE9choirs sous vide</strong></p>\n          <p class='pe-3 ps-3'>Les s\xE9choirs sous vide : du fait de leur conception cylindrique, les capacit\xE9s utiles sont r\xE9duites. Les temp\xE9ratures de fonctionnement sont g\xE9n\xE9ralement comprises entre 40 et 80\xB0C, d\u2019o\xF9 la n\xE9cessit\xE9 d\u2019une chaudi\xE8re.</p>\n          <p class='pe-3 ps-3'>\u2022  A vide continu : le bois repose sur des plateaux parcourus par de l\u2019eau chaude. Des plateaux de refroidissement sont dispos\xE9s sur les c\xF4t\xE9s pour condenser la vapeur d\u2019eau d\xE9gag\xE9e du bois</p>\n          <p class='pe-3 ps-3'>\u2022  A vide discontinu : le bois est empil\xE9 sur des baguettes. Le chauffage est obtenu par circulation d\u2019eau chaude \xE0 l\u2019int\xE9rieur de la paroi de la cellule. Il y a alternance entre les phases de r\xE9chauffage \xE0 la pression atmosph\xE9rique et les phases de s\xE9chage sous vide.</p>\n\t";
};

hideFaq4 = function hideFaq4() {
  document.getElementById("faq4").innerHTML = "";
};

faq5 = function faq5() {
  document.getElementById("faq5").innerHTML = "ms-3e et l\u2019humidit\xE9 de l\u2019air tout au long des \xE9tapes du cycle de s\xE9chage. Ces conditions de temp\xE9rature et d\u2019humidit\xE9 sont contr\xF4l\xE9es \xE0 l\u2019aide de capteurs plac\xE9s dans l\u2019\xE9coulement d\u2019air. La progression du s\xE9chage est quant \xE0 elle surveill\xE9e \xE0 l\u2019aide de capteurs (humidit\xE9 ou temp\xE9rature) plac\xE9s dans des planches t\xE9moins dispos\xE9es dans le s\xE9choir ou \xE0 l\u2019aide d\u2019un peson permettant de suivre l\u2019humidit\xE9 moyenne de la charge de bois. Dans le pass\xE9, la conduite du s\xE9chage \xE9tait manuelle. Avec l\u2019\xE9volution des techniques et des exigences du march\xE9, la conduite enti\xE8rement manuelle a \xE9t\xE9 automatis\xE9e. Actuellement, la conduite du s\xE9choir est soit semi-automatique ou automatique. Dans le premier cas, l\u2019op\xE9rateur intervient afin de changer les consignes d\u2019humidit\xE9 et de temp\xE9rature au fur et \xE0 mesure de la progression du s\xE9chage. Dans le deuxi\xE8me cas, le cycle de s\xE9chage peut \xEAtre programm\xE9 de sorte que les consignes de temp\xE9rature et d\u2019humidit\xE9 soient ajust\xE9es automatiquement au cours de ce cycle. L\u2019\xE9volution de celui-ci est contr\xF4l\xE9e soit par le temps soit par la mesure de l\u2019humidit\xE9 du bois. Il est cependant fortement recommand\xE9 d\u2019effectuer un contr\xF4le r\xE9gulier, de l\u2019ordre d\u2019une \xE0 deux fois par jour, d\u2019une part pour s\u2019assurer du bon fonctionnement des capteurs et, d\u2019autre part, pour ajuster \xE9ventuellement les param\xE8tres en cours de s\xE9chage.\n\t</p>\n\t";
};

hideFaq5 = function hideFaq5() {
  document.getElementById("faq5").innerHTML = "";
};

faq6 = function faq6() {
  document.getElementById("faq6").innerHTML = "\n\t<p class='pe-3 ps-3'>\n\n\tL\u2019op\xE9ration de s\xE9chage peut se d\xE9composer en cinq phases :<br><br>\n\t\n\t1-la mont\xE9e en temp\xE9rature : pendant laquelle la temp\xE9rature de l\u2019air est ramen\xE9e jusqu\u2019\xE0 la valeur souhait\xE9e pour le d\xE9but de la phase de s\xE9chage. Afin d\u2019\xE9viter le risque d'apparition des fentes en surface du bois, l\u2019humidit\xE9 relative de l\u2019air doit \xEAtre maintenue \xE0 une valeur suffisamment \xE9lev\xE9e,<br>\n\t\n\t2-le r\xE9chauffage : en l\u2019absence de gradient d\u2019humidit\xE9 dans le bois, l\u2019eau circule des zones chaudes vers les zones froides s\u2019opposant ainsi \xE0 la circulation de l\u2019eau des zones internes vers la surface. Il est donc indispensable, avant que le s\xE9chage, proprement dit commence, que le bois soit r\xE9chauff\xE9 dans toute sa masse. Cette phase doit \xEAtre effectu\xE9e en atmosph\xE8re tr\xE8s humide,<br>\n\t\n\t3-s\xE9chage : permet d\u2019abaisser la teneur en eau du bois jusqu\u2019\xE0 la teneur en eau finale d\xE9sir\xE9e. Il est r\xE9alis\xE9 en se basant sur les tables de s\xE9chage qui donnent, pour chaque essence et \xE9paisseur, les conditions de l\u2019air \xE0 utiliser en fonction de l\u2019humidit\xE9 du bois. Ces tables, bas\xE9es sur des connaissances empiriques, permettent d\u2019obtenir la dur\xE9e du s\xE9chage la plus courte possible tout en pr\xE9servant la qualit\xE9 du bois s\xE9ch\xE9.<br>\n\t\n\t4-\xE9quilibrage : en fin de s\xE9chage, l\u2019humidit\xE9 \xE0 c\u0153ur est plus importante que l\u2019humidit\xE9 en surface. Cette phase a donc pour but d\u2019\xE9quilibrer le taux d\u2019humidit\xE9 dans l\u2019\xE9paisseur du bois. L\u2019\xE9quilibrage permet \xE9galement de diminuer les contraintes m\xE9caniques r\xE9siduelles.<br>\n\t\n\t5-refroidissement : suite \xE0 l\u2019\xE9quilibrage, le bois doit subir un refroidissement progressif avant d\u2019\xEAtre sorti du s\xE9choir et ce afin d\u2019\xE9viter un choc thermique qui pourrait provoquer des fentes en surface.\n\t</p>\n\t";
};

hideFaq6 = function hideFaq6() {
  document.getElementById("faq6").innerHTML = "";
};

/***/ }),

/***/ "./src/app.scss":
/*!**********************!*\
  !*** ./src/app.scss ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					result = fn();
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/dist/app": 0,
/******/ 			"dist/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			for(moduleId in moreModules) {
/******/ 				if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 					__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 				}
/******/ 			}
/******/ 			if(runtime) var result = runtime(__webpack_require__);
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["dist/app"], () => (__webpack_require__("./src/app.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["dist/app"], () => (__webpack_require__("./src/app.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;