<?php

use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Index');
});

Route::get('/quiSommesNous', function () {
    return view('QuiSommesNous');
});

Route::get('/notreHistoire', function () {
    return view('NotreHistoire');
});

Route::get('/technologie', function () {
    return view('Technologie');
});

Route::get('/siteDeProduction', function () {
    return view('SiteDeProduction');
});


Route::get('contactAjout', 'App\Http\Controllers\ContactController@contact');

Route::get('cahierAjout', 'App\Http\Controllers\ContactController@cahierDesCharges');

Route::get('/contact', function () {
    return view('Contact');
});

Route::get('cahierAjout', 'App\Http\Controllers\ContactController@cahierDesCharges');

Route::get('/cahierDesCharges', function () {
    return view('CahierDesCharges');
});

Route::get('/societeFiliale', function () {
    return view('SocieteFiliale');
});

Route::get('/produits', function () {
    return view('Produits');
});

Route::get('/maintenance', function () {
    return view('Maintenance');
});


Route::get('/engagements', function () {
    return view('Engagements');
});

Route::get('/faq', function () {
    return view('Faq');
});

Route::get('/legales', function () {
    return view('Legales');
});

Route::get('/politique', function () {
    return view('Politique');
});

Route::get('/send-email',[MailController::class,'sendEmail']);