<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Contacts;
use App\Mail\TestMail;
use App\Mail\CahierMail;
use App\Models\CahierDesCharges;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ContactController extends BaseController
{
    public function contact(Request $request)
    {
        $nom = $request->input('nom');
        $prenom = $request->input('prenom');
        $mail = $request->input('mail');
        $telephone = $request->input('telephone');
        $entreprise = $request->input('entreprise');
        $cp = $request->input('cp');
        $message = $request->input('message');


        $contactClient = new Contacts;
        $contactClient-> nom= $nom;
        $contactClient-> prenom= $prenom;
        $contactClient-> mail = $mail;
        $contactClient-> telephone = $telephone;
        $contactClient-> entreprise = $entreprise;
        $contactClient-> cp = $cp;
        $contactClient-> message = $message;
        $contactClient->save();


        $details=[
            'title' => "Mr {$nom}" ,
            'Nom'=>"Nom : {$nom}",
            'Prenom' => "Prenom : {$prenom}",
            'Mail' => "Mail : {$mail}",
            'Telephone'=>"Téléphone : {$telephone}",
            'Entreprise' =>"Entreprise : {$entreprise}" ,
            'CodePostale' => "Code postale : {$cp}",
            'Message' => "Message : {$message}"
                 
           
        ];

        Mail::to("boulalaski@gmail.com")->send(new TestMail($details));
        
        return view('ContactAjout');

    }

    public function cahierDesCharges(REQUEST $request)
    {
        $tableauEssence = $request->input('listeESS');
        $tableauPlot = $request->input('plotArrive');
        $tableauEpMax = $request->input('epMax');
        $tableauLongueurMax = $request->input('longueurMax');
        $tableauHumidteDepart = $request->input('humiditeDepart');
        $tableauHumiditeFinale = $request->input('humiditeFinale');
        $tableauVolumeSechage = $request->input('volumeSechage');

        $nom = $request->input('nom');
        $prenom = $request->input('prenom');
        $mail = $request->input('mail');
        $telephone = $request->input('telephone');
        $entreprise = $request->input('entreprise');
        $cp = $request->input('cp');
        $option= $request->input('option');
        $infoCellule= $request->input('infoCellule');
        $approvisionnement = $request->input('approvisionnement');
        $hauteurChariot = $request->input('hauteurChariot');
        $autre = $request->input('autre');
        $hauteurBois = $request->input('hauteurBois');
        $largeurBois = $request->input('largeurBois');
        $longueurBois = $request->input('longueurBois');
        
        $listeESS1 = $request->input('listeESS')[0];
        $pLotArrive1 = $request->input('plotArrive')[0];
        $epMax1 = $request->input('epMax')[0];
        $longueurMax1 = $request->input('longueurMax')[0];
        $humiditeDepart1 = $request->input('humiditeDepart')[0];
        $humiditeFinale1 = $request->input('humiditeFinale')[0];
        $volumeSechage1 = $request->input('volumeSechage')[0];
       
        $listeESS2 = array_key_exists (1 , $tableauEssence ) ? $tableauEssence[1] : "";
        $pLotArrive2 = array_key_exists (1 , $tableauPlot ) ? $tableauPlot[1] : "";
        $epMax2 = array_key_exists (1 , $tableauEpMax ) ? $tableauEpMax[1] : "";
        $longueurMax2 = array_key_exists (1 , $tableauLongueurMax ) ? $tableauLongueurMax[1] : "";
        $humiditeDepart2 = array_key_exists (1 , $tableauHumidteDepart ) ? $tableauHumidteDepart[1] : "";
        $humiditeFinale2 = array_key_exists (1 , $tableauHumiditeFinale ) ? $tableauHumiditeFinale[1] : "";
        $volumeSechage2 = array_key_exists (1 , $tableauVolumeSechage ) ? $tableauVolumeSechage[1] : "";
     
        $listeESS3 = array_key_exists (2 , $tableauEssence ) ? $tableauEssence[2] : "";
        $pLotArrive3 = array_key_exists (2 , $tableauPlot ) ? $tableauPlot[2] : "";
        $epMax3 = array_key_exists (2 , $tableauEpMax ) ? $tableauEpMax[2] : "";
        $longueurMax3 = array_key_exists (2 , $tableauLongueurMax ) ? $tableauLongueurMax[2] : "";
        $humiditeDepart3 = array_key_exists (2 , $tableauHumidteDepart ) ? $tableauHumidteDepart[2] : "";
        $humiditeFinale3 = array_key_exists (2 , $tableauHumiditeFinale ) ? $tableauHumiditeFinale[2] : "";
        $volumeSechage3 = array_key_exists (2 , $tableauVolumeSechage ) ? $tableauVolumeSechage[2] : "";
        
        $listeESS4 = array_key_exists (3 , $tableauEssence ) ? $tableauEssence[3] : "";
        $pLotArrive4 = array_key_exists (3 , $tableauPlot ) ? $tableauPlot[3] : "";
        $epMax4 = array_key_exists (3 , $tableauEpMax ) ? $tableauEpMax[3] : "";
        $longueurMax4 = array_key_exists (3 , $tableauLongueurMax ) ? $tableauLongueurMax[3] : "";
        $humiditeDepart4 = array_key_exists (3 , $tableauHumidteDepart ) ? $tableauHumidteDepart[3] : "";
        $humiditeFinale4 = array_key_exists (3 , $tableauHumiditeFinale ) ? $tableauHumiditeFinale[3] : "";
        $volumeSechage4 = array_key_exists (3 , $tableauVolumeSechage ) ? $tableauVolumeSechage[3] : "";
       
        $typeAlim = $request->input('typeAlim');
        $tension = $request->input('tension');
        $choixMesure = $request->input('choixMesure');
        $nimp = $request->input('nimp');
        $enregistrement = $request->input('enregistrement');
        $message = $request->input('message');

        $cahierClient = new CahierDesCharges;
        $cahierClient-> nom= $nom;
        $cahierClient-> prenom= $prenom;
        $cahierClient-> mail = $mail;
        $cahierClient-> telephone = $telephone;
        $cahierClient-> entreprise = $entreprise;
        $cahierClient-> cp = $cp;
        $cahierClient-> option = $option;
        $cahierClient-> infoCellule = $infoCellule;
        $cahierClient-> approvisionnement = $approvisionnement;
        $cahierClient-> autre = $autre;
        $cahierClient-> hauteurChariot = $hauteurChariot;
        $cahierClient-> hauteurBois = $hauteurBois;
        $cahierClient-> largeurBois = $largeurBois;
        $cahierClient-> longueurBois = $longueurBois;
        $cahierClient-> listeESS1 = $listeESS1;
        $cahierClient-> plotArrive1 = $pLotArrive1;
        $cahierClient-> epMax1 = $epMax1; 
        $cahierClient-> longueurMax1 = $longueurMax1;
        $cahierClient-> humiditeDepart1 = $humiditeDepart1;
        $cahierClient-> humiditeFinale1 = $humiditeFinale1;
        $cahierClient-> volumeSechage1 = $volumeSechage1;
        $cahierClient-> listeESS2 = $listeESS2;
        $cahierClient-> plotArrive2 = $pLotArrive2;
        $cahierClient-> epMax2 = $epMax2; 
        $cahierClient-> longueurMax2 = $longueurMax2;
        $cahierClient-> humiditeDepart2 = $humiditeDepart2;
        $cahierClient-> humiditeFinale2 = $humiditeFinale2;
        $cahierClient-> volumeSechage2 = $volumeSechage2;
        $cahierClient-> listeESS3 = $listeESS3;
        $cahierClient-> plotArrive3 = $pLotArrive3;
        $cahierClient-> epMax3 = $epMax3; 
        $cahierClient-> longueurMax3 = $longueurMax3;
        $cahierClient-> humiditeDepart3 = $humiditeDepart3;
        $cahierClient-> humiditeFinale3 = $humiditeFinale3;
        $cahierClient-> volumeSechage3 = $volumeSechage3;
        $cahierClient-> listeESS4 = $listeESS4;
        $cahierClient-> plotArrive4 = $pLotArrive4;
        $cahierClient-> epMax4 = $epMax4; 
        $cahierClient-> longueurMax4 = $longueurMax4;
        $cahierClient-> humiditeDepart4 = $humiditeDepart4;
        $cahierClient-> humiditeFinale4 = $humiditeFinale4;
        $cahierClient-> volumeSechage4 = $volumeSechage4;
        $cahierClient-> typeAlim = $typeAlim;
        $cahierClient-> tension = $tension;
        $cahierClient-> choixMesure = $choixMesure;
        $cahierClient-> nimp = $nimp;
        $cahierClient-> enregistrement = $enregistrement;
        $cahierClient-> message = $message;
        $cahierClient->save();


        $details=[
   
            'title' => "Mr {$nom} à remplie le cahier des charges" ,
            'Nom'=>"Nom : {$nom}",
            'Prenom' => "Prenom : {$prenom}",
            'Mail' => "Mail : {$mail}",
            'Telephone'=>"Téléphone : {$telephone}",
            'Entreprise' =>"Entreprise : {$entreprise}" ,
            'CodePostale' => "Code postale : {$cp}",
            'Option' => "Mr {$nom} souhaite un/une : {$option}",
            'InfoCellule' => "La cellule sera : {$infoCellule}",
            'Approvisionnement' => "méthode d'approvisionnement : {$approvisionnement}",
            'Autre' => "Autre  : {$autre}",
            'HauteurChariot' => "Hauteur du chariot : {$hauteurChariot}",
            'HauteurBois' => "Hauteur du bois à sécher : {$hauteurBois}",
            'LargeurBois' => "Largeur du bois à sécher : {$largeurBois}",
            'LongueurBois' => "Longueur du bois à sécher : {$longueurBois}",
            'ListeESS1' => "listeESS : {$listeESS1}",
            'PLotArrive1' => "Plot/Arrive : {$pLotArrive1}",
            'EpMax1' => "Ep max : {$epMax1}",
            'LongueurMax1' => "Longueur max : {$longueurMax1}",
            'HumiditeDepart1' => "Humidité de départ : {$humiditeDepart1}",
            'HumiditeFinale1' => "Humidité finale : {$humiditeFinale1}",
            'VolumeSechage1' => "Volume à sécher : {$volumeSechage1}",
            'ListeESS2' => "listeESS : {$listeESS2}",
            'PLotArrive2' => "Plot/Arrive : {$pLotArrive2}",
            'EpMax2' => "Ep max : {$epMax2}",
            'LongueurMax2' => "Longueur max : {$longueurMax2}",
            'HumiditeDepart2' => "Humidité de départ : {$humiditeDepart2}",
            'HumiditeFinale2' => "Humidité finale : {$humiditeFinale2}",
            'VolumeSechage2' => "Volume à sécher : {$volumeSechage2}",
            'ListeESS3' => "listeESS : {$listeESS3}",
            'PLotArrive3' => "Plot/Arrive : {$pLotArrive3}",
            'EpMax3' => "Ep max : {$epMax3}",
            'LongueurMax3' => "Longueur max : {$longueurMax3}",
            'HumiditeDepart3' => "Humidité de départ : {$humiditeDepart3}",
            'HumiditeFinale3' => "Humidité finale : {$humiditeFinale3}",
            'VolumeSechage3' => "Volume à sécher : {$volumeSechage3}",
            'ListeESS4' => "listeESS : {$listeESS4}",
            'PLotArrive4' => "Plot/Arrive : {$pLotArrive4}",
            'EpMax4' => "Ep max : {$epMax4}",
            'LongueurMax4' => "Longueur max : {$longueurMax4}",
            'HumiditeDepart4' => "Humidité de départ : {$humiditeDepart4}",
            'HumiditeFinale4' => "Humidité finale : {$humiditeFinale4}",
            'VolumeSechage4' => "Volume à sécher : {$volumeSechage4}",
            'TypeAlim' => "Type d'alimentation : {$typeAlim}",
            'Tension' => "Tension : {$tension}",
            'ChoixMesure' => "Choix des mesures : {$choixMesure}",
            'Nimp' => "Nimp : {$nimp}",
            'Enregistrement' => "Enregistement : {$enregistrement}",
            'Message' => "Message : {$message}"
                 
           
        ];

        Mail::to("boulalaski@gmail.com")->send(new CahierMail($details));
        
         return view('CahierAjout');

    }

}


