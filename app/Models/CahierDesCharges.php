<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CahierDesCharges extends Model
{
    use HasFactory;
    protected $fillable = [
        'nom', 'prenom', 'mail','telephone', 'entreprise', 'cp','option','infoCellule','approvisionnement','autre','hauteurChariot','hauteurBois','largeurBois','longueurBois','listeESS1','plotArrive1','epMax1','longueurMax1','humiditeDepart1','humiditeFinale1','volumeSechage1','listeESS2','plotArrive2','epMax2','longueurMax2','humiditeDepart2','humiditeFinale2','volumeSechage2','listeESS3','plotArrive3','epMax3','longueurMax3','humiditeDepart3','humiditeFinale3','volumeSechage3','listeESS4','plotArrive4','epMax4','longueurMax4','humiditeDepart4','humiditeFinale4','volumeSechage4','typeAlim','tension','choixMesure','nimp','enregistrement','message',
    ];
}
