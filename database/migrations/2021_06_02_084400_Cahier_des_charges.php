<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cahierdescharges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cahier_des_charges', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('prenom');
            $table->string('mail');
            $table->string('telephone');
            $table->string('entreprise');
            $table->string('cp');
            $table->string('option');
            $table->string('infoCellule')->nullable();
            $table->string('approvisionnement')->nullable();
            $table->string('autre')->nullable();
            $table->string('hauteurChariot')->nullable();
            $table->string('hauteurBois')->nullable();
            $table->string('largeurBois')->nullable();
            $table->string('longueurBois')->nullable();
            $table->string('listeESS1')->nullable();
            $table->string('plotArrive1')->nullable();
            $table->string('epMax1')->nullable();
            $table->string('longueurMax1')->nullable();
            $table->string('humiditeDepart1')->nullable();
            $table->string('humiditeFinale1')->nullable();
            $table->string('volumeSechage1')->nullable();
            $table->string('listeESS2');
            $table->string('plotArrive2');
            $table->string('epMax2');
            $table->string('longueurMax2');
            $table->string('humiditeDepart2');
            $table->string('humiditeFinale2');
            $table->string('volumeSechage2');
            $table->string('listeESS3');
            $table->string('plotArrive3');
            $table->string('epMax3');
            $table->string('longueurMax3');
            $table->string('humiditeDepart3');
            $table->string('humiditeFinale3');
            $table->string('volumeSechage3');
            $table->string('listeESS4');
            $table->string('plotArrive4');
            $table->string('epMax4');
            $table->string('longueurMax4');
            $table->string('humiditeDepart4');
            $table->string('humiditeFinale4');
            $table->string('volumeSechage4');
            $table->string('typeAlim');
            $table->string('tension');
            $table->string('choixMesure');
            $table->string('nimp');
            $table->string('enregistrement');
            $table->string('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
