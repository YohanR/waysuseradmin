<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">


    <title>Demande de cahier des charges</title>
</head>
<body>

<h1>{{$details['title']}}</h1>
<br>
<p class='h3'><strong><u>Contact<u></strong></p>
<table>
<tr>
<td><p>{{$details['Nom']}}</P></td>
<td><p>{{$details['Prenom']}}</P></td>   
</tr>
<tr>
<td><p>{{$details['Mail']}}</P></td>
<td><p>{{$details['Telephone']}}</P></td>   
</tr>
<tr>
<td><p>{{$details['CodePostale']}}</P></td>
<td><p>{{$details['Entreprise']}}</P></td>   
</tr>
</table>
<br>
<p class='h3'><strong><u>Informations choix clients<u></strong></p>
<table>
<tr>
<td><p>{{$details['Option']}}</P></td>
</tr>
<tr>
@if (($details['InfoCellule']!="La cellule sera : "))
<td><p>{{$details['InfoCellule']}}</P></td>   
@endif
</tr>
<tr>
@if (($details['Approvisionnement']!="méthode d'approvisionnement : "))
<td><p>{{$details['Approvisionnement']}}</P></td>
@endif
@if (($details['Autre']!="Autre  : "))
<td><p>{{$details['Autre']}}</P></td>   
@endif
</tr>
<tr>
@if (($details['HauteurChariot']!="Hauteur du chariot : "))
<td><p>{{$details['HauteurChariot']}}</P></td>
@endif
@if (($details['HauteurBois']!="Hauteur du bois à sécher : "))
<td><p>{{$details['HauteurBois']}}</P></td> 
@endif  
</tr>
<tr>
@if (($details['LargeurBois']!="Largeur du bois à sécher : "))
<td><p>{{$details['LargeurBois']}}</P></td>
@endif
@if (($details['LongueurBois']!="Longueur du bois à sécher : "))
<td><p>{{$details['LongueurBois']}}</P></td>   
@endif
</tr>
</table>
<table>
<br>
<p class='h3'><strong><u>Choix des essences 1<u></strong></p>
<tr>
@if (($details['ListeESS1']!="listeESS : "))
<td><p>{{$details['ListeESS1']}}</P></td>
@endif
@if (($details['PLotArrive1']!="Plot/Arrive : "))
<td><p>{{$details['PLotArrive1']}}</P></td>   
@endif
</tr>
<tr>
@if (($details['EpMax1']!="Ep max : "))
<td><p>{{$details['EpMax1']}}</P></td>
@endif
@if (($details['LongueurMax1']!="Longueur max : "))
<td><p>{{$details['LongueurMax1']}}</P></td>  
@endif
</tr>
<tr>
@if (($details['HumiditeDepart1']!="Humidité de départ : "))
<td><p>{{$details['HumiditeDepart1']}}</P></td>
@endif
</tr>
<tr>
@if (($details['HumiditeFinale1']!="Humidité finale : "))
<td><p>{{$details['HumiditeFinale1']}}</P></td>  
@endif 
</tr>
<tr>
@if (($details['VolumeSechage1']!="Volume à sécher : "))
<td><p>{{$details['VolumeSechage1']}}</P></td>
@endif
</table>
<br>
@if (($details['ListeESS2']!="listeESS : "))
<p class='h3'><strong><u>Choix des essences 2<u></strong></p>
@endif
<tr>
@if (($details['ListeESS2']!="listeESS : "))
<td><p>{{$details['ListeESS2']}}</P></td>
@endif
@if (($details['PLotArrive2']!="Plot/Arrive : "))
<td><p>{{$details['PLotArrive2']}}</P></td>   
@endif
</tr>
<tr>
@if (($details['EpMax2']!="Ep max : "))
<td><p>{{$details['EpMax2']}}</P></td>
@endif
@if (($details['LongueurMax2']!="Longueur max : "))
<td><p>{{$details['LongueurMax2']}}</P></td>  
@endif
</tr>
<tr>
@if (($details['HumiditeDepart2']!="Humidité de départ : "))
<td><p>{{$details['HumiditeDepart2']}}</P></td>
@endif
</tr>
<tr>
@if (($details['HumiditeFinale2']!="Humidité finale : "))
<td><p>{{$details['HumiditeFinale2']}}</P></td>  
@endif 
</tr>
<tr>
@if (($details['VolumeSechage2']!="Volume à sécher : "))
<td><p>{{$details['VolumeSechage2']}}</P></td>
@endif
</table>
<br>
@if (($details['ListeESS3']!="listeESS : "))
<p class='h3'><strong><u>Choix des essences 3<u></strong></p>
@endif
<tr>
@if (($details['ListeESS3']!="listeESS : "))
<td><p>{{$details['ListeESS3']}}</P></td>
@endif
@if (($details['PLotArrive3']!="Plot/Arrive : "))
<td><p>{{$details['PLotArrive3']}}</P></td>   
@endif
</tr>
<tr>
@if (($details['EpMax3']!="Ep max : "))
<td><p>{{$details['EpMax3']}}</P></td>
@endif
@if (($details['LongueurMax3']!="Longueur max : "))
<td><p>{{$details['LongueurMax3']}}</P></td>  
@endif
</tr>
<tr>
@if (($details['HumiditeDepart3']!="Humidité de départ : "))
<td><p>{{$details['HumiditeDepart3']}}</P></td>
@endif
</tr>
<tr>
@if (($details['HumiditeFinale3']!="Humidité finale : "))
<td><p>{{$details['HumiditeFinale3']}}</P></td>  
@endif 
</tr>
<tr>
@if (($details['VolumeSechage3']!="Volume à sécher : "))
<td><p>{{$details['VolumeSechage3']}}</P></td>
@endif
</table>
<br>
@if (($details['ListeESS4']!="listeESS : "))
<p class='h3'><strong><u>Choix des essences 4<u></strong></p>
@endif
<tr>
@if (($details['ListeESS4']!="listeESS : "))
<td><p>{{$details['ListeESS4']}}</P></td>
@endif
@if (($details['PLotArrive4']!="Plot/Arrive : "))
<td><p>{{$details['PLotArrive4']}}</P></td>   
@endif
</tr>
<tr>
@if (($details['EpMax4']!="Ep max : "))
<td><p>{{$details['EpMax4']}}</P></td>
@endif
@if (($details['LongueurMax4']!="Longueur max : "))
<td><p>{{$details['LongueurMax4']}}</P></td>  
@endif
</tr>
<tr>
@if (($details['HumiditeDepart4']!="Humidité de départ : "))
<td><p>{{$details['HumiditeDepart4']}}</P></td>
@endif
</tr>
<tr>
@if (($details['HumiditeFinale4']!="Humidité finale : "))
<td><p>{{$details['HumiditeFinale4']}}</P></td>  
@endif 
</tr>
<tr>
@if (($details['VolumeSechage4']!="Volume à sécher : "))
<td><p>{{$details['VolumeSechage4']}}</P></td>
@endif
</table>
<br>
<p class='h3'><strong><u>Alimentation éléctrique<u></strong></p>
<table>
<tr>
<td><p>{{$details['TypeAlim']}}</P></td>   
</tr>
<tr>
<td><p>{{$details['Tension']}}</P></td>
</tr>
</table>
<br>
<p class='h3'><strong><u>Regulateur<u></strong></p>
<table>
<tr>
<td><p>{{$details['ChoixMesure']}}</P></td>   
</tr>
<tr>
<td><p>{{$details['Nimp']}}</P></td>
</tr>
</table>
<br>
<p class='h3'><strong><u>Demande Client<u></strong></p>
<table>
<tr>
<td><p>{{$details['Enregistrement']}}</P></td>   
</tr>
</table>
<p><strong>{{$details['Message']}}<strong></p>


    <p>Cordialement,  <br>
    wAys</p>
</body>
</html>