<!doctype html>

<head>
    <title>@yield('title')</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Css additionel -->
    <link rel="stylesheet" href="css/app.css" />

    <!-- Bootstrap CSS -->
    <link href="dist/app.css" rel="stylesheet">

</head>

<body>

    <!-- navigation bar -->
<!-- navigation bar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-bottom: 25px;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown"style="margin-bottom: 20px;">
      <ul class="navbar-nav">
        <li class="nav-item active pt-2 ms-3">
          <a class="nav-link" style="padding-top: 50px;" href="/">Accueil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px;" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Groupe wAys
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/quiSommesNous">Qui Sommes nous</a>
            <a class="dropdown-item" href="/technologie">Technologie</a>
            <a class="dropdown-item" href="/siteDeProduction">Site de Production</a>
            <a class="dropdown-item" href="/contact">Contact</a>
          </div>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px; padding-right:120px" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right:70px;">
            wAys Industrie
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/societeFiliale">Sociétés filiales</a>
            <a class="dropdown-item" href="/produits">Nos produits</a>
            <a class="dropdown-item" href="/maintenance">Maintenance</a>
            <a class="dropdown-item" href="/cahierDesCharges">Cahier des charges</a>
          </div>
        </li>
        <li class="nav-item ms-3 me-3 test2">
        <a class="nav-link d-none d-lg-block d-xl-block" href="/"><img class='logonav' src="{{asset('/img/Logo Ways Noir.png')}}" alt="Logo ways" style="position: absolute;
    margin-top: -17px;
    margin-left: -44px;
    z-index: 1;
    width:140px;
    height:auto"  ></a>
        </li> 
        <li class="nav-item pt-2 ms-3 me-3 " >
          <a class="nav-link" style="padding-top: 50px; padding-left:120px" href="/engagements"  style="margin-left:70px;">Nos engagements</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3 ">
          <a class="nav-link" style="padding-top: 50px;" href="/faq">FAQ</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3">
          <a class="nav-link" style="padding-top: 50px;" href="/contact">Nous contacter</a>
        </li>
      </ul>
    </div>
  </nav>
    <!-- navigation bar ends here -->
    @yield('content')


    <div class="containerimage">
  <img class='imageTop' src="{{asset('/img/maisonBois.jpg')}}" alt="Cinque Terre" width="1000" height="300">
  <div class="topleftimagetxt text-white"><stron><p class='h1 titrePage'>Innovations<br> technologiques</p></strong></div>
  </div>

    <div class='d-flex mt-4'>
        <div class='col-0 col-lg-1'>
        </div>
        <div class='col-12 col-lg-10'>
            <div class='bg-secondary z-indextec'>
                <p class='text-center h2 text-white pt-3  mt-3'>Technlogies</p>
                <p class=' ms-5 me-5 text-white pb-3'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed neque lacus, pharetra nec sollicitudin vitae, commodo vitae elit. Nulla facilisi. Nulla accumsan eget elit ac egestas. Cras malesuada, orci condimentum placerat rhoncus, augue sapien placerat dolor, quis scelerisque risus dolor in ante. Nulla eu commodo ipsum. Etiam sit amet pellentesque magna. Mauris gravida, risus dictum viverra vestibulum, augue mauris venenatis nulla, quis condimentum eros
                    ante et nulla.<br><br> Suspendisse placerat odio eu metus porttitor consectetur sit amet pulvinar lacus. Ut lectus velit, semper non sollicitudin vel, ultrices a dolor. Etiam eu erat et mauris ultricies tempor a at dolor. Suspendisse ultrices, justo a porttitor tristique, neque erat porta augue, eu lacinia lorem turpis nec nisi. Nunc nec orci libero. Nullam semper vitae ex tincidunt condimentum.</p>
            </div>
        </div>
        <div class='col-0 col-lg-1'>
        </div>
    </div>


    <!-- Design techno -->
    <div class='bg-dark h-250px marginTopTechno'>
    <img class='d-none d-lg-block d-xl-block  ' src="{{asset('/img/engagements.jpg')}}" alt="Cinque Terre" height="100%" width="100%" >
    </div>
    <!-- image séchage -->
    <div class='text-center z-indextec'>
        <img class='img-fluid w-75 WIDTH100 marginTopTechnoSechage  ' src="{{asset('/img/enjeudusechage.webp')}}" alt='un morceau de bois'>
    </div>
    <div class='bg-dark h-250px marginTopImage'>
    <img class='d-none d-lg-block d-xl-block  ' src="{{asset('/img/engagements.jpg')}}" alt="Cinque Terre" height="100%" width="100%" >
    </div>

    <div class='d-flex g-0  mt-3'>
        <div class='col-0 col-lg-1'>
        </div>
        <div class='col-12 col-lg-10 '>
            <div class='bg-secondary margintopSuiteHistoire z-indextec'>
                <p class=' ms-5 me-5 text-white pb-3 pt-3'>C'est la solution qui se rapproche le plus des conditions de séchage naturels, tant sur la déformation des bois que sur la qualité générale du bois obtenu. Ce système de pompe à chaleur air/air permet thermodynamiquement de sécher <br><br>
                    L'avantage majeur d'une solution comme celle-ci est qu'il faut 3 fois moins de puissance électrique que de puissance calorifique pour sécher le bois. En effet, l'énergie à produire pour sécher le bois est colossale car elle se compose de 3 ace et l'extraction de cette dernière au contact de l'air / les déperditions ).
                    <br><br> C'est pourquoi, notre cahier des charges est fait pour que vous nous exprimiez votre besoin et nous y apportons une solution sur mesure ainsi qu'un dimensionnement optimal.
                </p>
            </div>
        </div>
        <div class='col-0 col-lg-1'>
        </div>
    </div>


    <div class="d-flex g-0  row h-900px mt-4  pb-5 ">
        <div class="col-12 col-lg-5   ">
            <div class='d-flex g-0  row '>
                <div class='col-1 col-lg-0'>
                </div>
                <div class='col-10 col-lg-12 padding bg-secondary borderBottomLeftTechno  '>
                    <img class='img-fluid' width='120%' src="{{asset('/img/atout.webp')}}" alt='un morceau de bois'>
                </div>
                <div clas=' col-0'>
                    <BR><BR>
                </div>
                <div class='col-12 z-indextec  bg-secondary borderTopRightTechno '>
                    <p class='h2 text-center text-white pt-3 pb-1 '>Histoire</p>
                    <p class='text-white ms-5 me-5'>En 2020 L'entité séchage par déshumidification est créee par Guillaume CARMASSI et se nomme Ecotec Séchage. Il souhaite mettre l'accent sur les nouvelles technologies et répondre au mieux aux problématiques dont font face les entreprises du bois aujourd'hui. Issu d'une école d'ingénieurs , il acquis de solides compétences sur la transformation du bois ( 1 ere et seconde transformation) en tant que responsable méthodes et bureau d'études.
                        <br><br>Une des décisions fortes a été de se relancer au travers du salon Eurobois à Lyon en février dernier. Il a décidé de créer sa société mais tout en gardant le logo de la société Poix en héritage transmis par Jacquec ROUGET à son tour, gage de qualités.
                    </p>
                </div>
                <div class='col-1 col-lg-0'>
                </div>
            </div>
        </div>
        <div class="col-0 col-lg-2">
        </div>
        <div class="  col-12 col-lg-5 bg-secondary borderTopLeftNorme ">
            <p class='h2 text-center text-white pt-3 pb-3'>Norme n20</p>
            <p class='text-white ms-5 me-5'>Séché jusqu'à 0%, le bois devient inerte et n'est donc plus sensible aux insectes ni aux champignons. Sans traitement, il devient plus durable et plus facile à recycler en fin de vie car non souillé.
                Séché jusqu'à 0%, le bois
                <br><br>devient inerte et n'est donc plus sensible aux insectes ni aux champignons. Sans traitement, il devient plus durable et plus facile à recycler en fin de vie car non souillé.s
                <br><br>Lors de la survenance de tempêtes ou d'invasions
                d'insectes ou de parasites, pouvoir sécher le bois jusqu'à 0% permettra de le stocker sans risque et sans impacter son cours..
                <br><br>Séché jusqu'à 0%, le bois devient inerte et n'est donc plus sensible aux insectes ni
                <br><br>Lors de la survenance de tempêtes ou d'invasions
                d'insectes ou de parasites, pouvoir sécher le bois jusqu'à 0% permettra de le stocker ..
                <br><br>Lors de la survenance de tempêtes ou d'invasions
                d'insectes ou de parasites, pouvoir sécher le bois jusqu'à 0% permettra de le stocker sans risque et sans impacter son cours..
            </p>
        </div>
    </div>

    <footer class='pt-2 pb-2  mt-5 marfoot h-270px mediafooth' style="background: #452201">
      <div class="d-flex  rowmedia text-white">
        <div class="col-0 col-lg-2 ">
          <img class='ms-4 logonav d-none d-lg-block d-xl-block ' src="{{asset('/img/Logo industrie blanc png.png')}}">
        </div>
        <div class="col-6 col-lg-5 W-100percent">
          <div class="d-flex row mlfoot mt-3">
            <div class="col-12">
              <h1>Groupe wAys</h1>
            </div>
            <div class="col-12">
              <p>6 rue du Puits Bardin</p>
            </div>
            <div class="col-12">
              <p>77920 SAMOIS SUR SEINE</p>
            </div>
            <div class="col-12">
              <p>tél : 06.67.66.10.28</p>
            </div>
            <div class="col-12">
              <p>Mail : contact@w-ays.com</p>
            </div>
          </div>
        </div>
        <div class="col-0 col-lg-1 text-white">
          <span class=" d-none d-lg-block d-xl-block vertical-line"></span>
        </div>
        <div class="col-6 col-lg-4  displaynone mttopfootmedia W-100percent">
          <div class="d-flex  row">
            <div class="col-12 pt-5">
              <h1>Contactez nous</h1>
            </div>
            <div class="col-12 pt-5">
              <h1>Suivez nous aussi (logo)</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="d-flex text-white text-center displaynone2  row">
        <div class="col-6 pt-5">
          <h1>Contactez nous</h1>
        </div>
        <div class="col-6 pt-5">
          <h1>Suivez nous aussi (logo)</h1>
        </div>
      </div>
      <div class='d-flex text-white row pt-3 pb-3'>
        <div class="col-4">

        </div>
        <div class=" col-2">
          <a href="/legales" style="color: #8ebf42" >Mention légales</a>
        </div>
        <div class='col-2 '>
          <a href="/politique" style="color: #8ebf42" >Politique de confidentialité</a>
        </div>
        <div class="col-4">
          
          </div>
      </div>

    </footer>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="dist/app.js" type="text/javascript"></script>




</body>

</html>