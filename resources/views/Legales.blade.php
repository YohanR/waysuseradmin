<!doctype html>

<head>
  <title>@yield('title')</title>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Css additionel -->
  <link rel="stylesheet" href="css/app.css" />

  <!-- Bootstrap CSS -->
  <link href="dist/app.css" rel="stylesheet">

</head>

<body>

  <!-- navigation bar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-bottom: 25px;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown"style="margin-bottom: 20px;">
      <ul class="navbar-nav">
        <li class="nav-item active pt-2 ms-3">
          <a class="nav-link" style="padding-top: 50px;" href="/">Accueil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px;" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Groupe wAys
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/quiSommesNous">Qui Sommes nous</a>
            <a class="dropdown-item" href="/technologie">Technologie</a>
            <a class="dropdown-item" href="/siteDeProduction">Site de Production</a>
            <a class="dropdown-item" href="/contact">Contact</a>
          </div>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px; padding-right:120px" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right:70px;">
            wAys Industrie
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/societeFiliale">Sociétés filiales</a>
            <a class="dropdown-item" href="/produits">Nos produits</a>
            <a class="dropdown-item" href="/maintenance">Maintenance</a>
            <a class="dropdown-item" href="/cahierDesCharges">Cahier des charges</a>
          </div>
        </li>
        <li class="nav-item ms-3 me-3 test2">
        <a class="nav-link d-none d-lg-block d-xl-block" href="/"><img class='logonav' src="{{asset('/img/Logo Ways Noir.png')}}" alt="Logo ways" style="position: absolute;
    margin-top: -17px;
    margin-left: -44px;
    z-index: 1;
    width:140px;
    height:auto"  ></a>
        </li> 
        <li class="nav-item pt-2 ms-3 me-3 " >
          <a class="nav-link" style="padding-top: 50px; padding-left:120px" href="/engagements"  style="margin-left:70px;">Nos engagements</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3 ">
          <a class="nav-link" style="padding-top: 50px;" href="/faq">FAQ</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3">
          <a class="nav-link" style="padding-top: 50px;" href="/contact">Nous contacter</a>
        </li>
      </ul>
    </div>
  </nav>

  <!-- navigation bar ends here -->
  @yield('content')
  <h1 class="text-center pt-3 pb-3" style="color: #8ebf42"><u><strong>Mentions Légales</strong></u></h1>

  <div class='d-flex row'>
    <div class="col-1 col-lg-2">
    </div>
    <div class='col-10 col-lg-8'>
      <h1 style="color: #87CEEB">Informations société :</h1>
       <p> <strong>SAS wAys</strong> – 6 RUE DU PUITS BARDIN - 77920 SAMOIS SUR SEINE – FRANCE<br>
        Tél. : 06 65 41 55 77<br>
        RCS Melun 811 609 916</p>
       <h1 style="color: #87CEEB"> Responsabilité éditoriale :</h1>
        <p>Malgré tous nos contrôles, une erreur peut survenir dans les informations
        mentionnées (désignation, référence…). Celle-ci ne saurait engager notre
        responsabilité dans la mesure où seules les informations transmises aux points de
        vente qui distribuent nos produits font foi.<p>
        <h1 style="color: #87CEEB">Hébergement du site :</h1>
       <p> HOSTINGER</p>
       <h1 style="color: #87CEEB"> Avis relatif à la sécurité :</h1>
        <p>Afin d&#39;assurer sa sécurité et de garantir son accès à tous, ce site Internet utilise des
        logiciels pour contrôler les flux sur le site, pour identifier les tentatives non autorisées
        de connexion ou de changement de l&#39;information, ou toute autre initiative pouvant
        causer des dommages. Les tentatives non autorisées de chargement d&#39;information,
        d&#39;altération des informations, visant à causer un dommage et d&#39;une manière générale
        toute atteinte à la disponibilité et l&#39;intégrité de ce site sont strictement interdites et
        passibles de sanctions pénales. Le fait d&#39;accéder ou de se maintenir
        frauduleusement, dans tout ou partie d&#39;un système de traitement automatisé de
        données est puni de deux ans d&#39;emprisonnement et de 30 000 Euros d&#39;amende
        (article 323-1 du code pénal). Le fait d&#39;introduire frauduleusement des données dans
        un système de traitement automatisé ou de supprimer ou de modifier
        frauduleusement les données qu&#39;il contient est puni de cinq ans d&#39;emprisonnement et
        de 75 000 Euros d&#39;amende (article 323-3 du code pénal).</p>
        <h1 style="color: #87CEEB">Politique de confidentialité</h1>
        <p>Pour connaître notre politique de confidentialité sur les données personnelles,
        veuillez consulter la page suivante : <a href="/politique" >Politique de confidentialité</a>.<p>
        <h1 style="color: #87CEEB">Propriété - Copyright :</h1>
        <p>La présentation et le contenu du site <strong>www.w-ays.com</strong> constituent, ensemble, une
        œuvre protégée par les lois en vigueur des droits d’auteur, de la propriété
        intellectuelle et industrielle. Les dénominations ou appellations, les logos, sont des
        marques déposées. Les créations apparaissant éventuellement sur le site, dessins,
        photographies, images, textes, séquences animées sonores ou non et autres
        documentations représentées, sont objets de droits d’auteur, de propriété industrielle
        et/ou intellectuelle et sont, selon les cas, propriété de la société SAS wAys ou de
        tiers ayant autorisé la société SAS wAys à les utiliser. Aucune exploitation
        commerciale, reproduction, représentation, utilisation, adaptation, modification,

        incorporation, traduction, commercialisation, partielle ou intégrale de ces éléments (y
        compris ceux téléchargeables ou copiables) ne pourra en être faite sans l&#39;accord
        préalable et écrit de la société SAS wAys, à l&#39;exception de l&#39;utilisation pour un usage
        privé sous réserve des dispositions différentes voire plus restrictives du Code de la
        propriété intellectuelle. Outre les droits d’auteurs, la violation de l&#39;un de ces droits de
        propriété industrielle et/ou intellectuelle est un délit de contrefaçon notamment
        passible de 3 ans d&#39;emprisonnement et d&#39;une amende de 300 000 euros.</p>
        <h1 style="color: #87CEEB">Réalisation du site</h1>
       <p> Le site internet <strong>www.w-ays.com</strong> est une réalisation de la Direction des Systèmes
        d&#39;Informations de la SAS wAys
        Nos partenaires dans la réalisation du site sont les suivants :
        Webdesign.
        Intégration HTML5 / Responsive.
        Intégration CMS.
        Développements sur mesure.
        Crédit photo</p>
    </div>
    <div class="col-1 col-lg-2">
    </div>
  </div>

  <footer class='pt-2 pb-2  mt-5  h-270px mediafooth' style="background: #452201">
      <div class="d-flex  rowmedia text-white">
        <div class="col-0 col-lg-2 ">
          <img class='ms-4 logonav d-none d-lg-block d-xl-block ' src="{{asset('/img/Logo industrie blanc png.png')}}">
        </div>
        <div class="col-6 col-lg-5 W-100percent">
          <div class="d-flex row mlfoot mt-3">
            <div class="col-12">
              <h1>Groupe wAys</h1>
            </div>
            <div class="col-12">
              <p>6 rue du Puits Bardin</p>
            </div>
            <div class="col-12">
              <p>77920 SAMOIS SUR SEINE</p>
            </div>
            <div class="col-12">
              <p>tél : 06.67.66.10.28</p>
            </div>
            <div class="col-12">
              <p>Mail : contact@w-ays.com</p>
            </div>
          </div>
        </div>
        <div class="col-0 col-lg-1 text-white">
          <span class=" d-none d-lg-block d-xl-block vertical-line"></span>
        </div>
        <div class="col-6 col-lg-4  displaynone mttopfootmedia W-100percent">
          <div class="d-flex  row">
            <div class="col-12 pt-5">
              <h1>Contactez nous</h1>
            </div>
            <div class="col-12 pt-5">
              <h1>Suivez nous aussi (logo)</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="d-flex text-white text-center displaynone2  row">
        <div class="col-6 pt-5">
          <h1>Contactez nous</h1>
        </div>
        <div class="col-6 pt-5">
          <h1>Suivez nous aussi (logo)</h1>
        </div>
      </div>
      <div class='d-flex text-white row pt-3 pb-3'>
        <div class="col-4">

        </div>
        <div class=" col-2">
          <a href="/legales" style="color: #8ebf42" >Mention légales</a>
        </div>
        <div class='col-2 '>
          <a href="/politique" style="color: #8ebf42" >Politique de confidentialité</a>
        </div>
        <div class="col-4">
          
          </div>
      </div>

    </footer>





  <!-- jQuery first, then Popper.js, then Bootstrap JS -->

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="dist/app.js" type="text/javascript"></script>




</body>

</html>