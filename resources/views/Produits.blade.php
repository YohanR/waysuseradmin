<!doctype html>

<head>
  <title>@yield('title')</title>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Css additionel -->
  <link rel="stylesheet" href="css/app.css" />

  <!-- Bootstrap CSS -->
  <link href="dist/app.css" rel="stylesheet">

</head>

<body>

  <!-- navigation bar -->
  <!-- navigation bar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-bottom: 25px;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown"style="margin-bottom: 20px;">
      <ul class="navbar-nav">
        <li class="nav-item active pt-2 ms-3">
          <a class="nav-link" style="padding-top: 50px;" href="/">Accueil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px;" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Groupe wAys
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/quiSommesNous">Qui Sommes nous</a>
            <a class="dropdown-item" href="/technologie">Technologie</a>
            <a class="dropdown-item" href="/siteDeProduction">Site de Production</a>
            <a class="dropdown-item" href="/contact">Contact</a>
          </div>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px; padding-right:120px" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right:70px;">
            wAys Industrie
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/societeFiliale">Sociétés filiales</a>
            <a class="dropdown-item" href="/produits">Nos produits</a>
            <a class="dropdown-item" href="/maintenance">Maintenance</a>
            <a class="dropdown-item" href="/cahierDesCharges">Cahier des charges</a>
          </div>
        </li>
        <li class="nav-item ms-3 me-3 test2">
        <a class="nav-link d-none d-lg-block d-xl-block" href="/"><img class='logonav' src="{{asset('/img/Logo Ways Noir.png')}}" alt="Logo ways" style="position: absolute;
    margin-top: -17px;
    margin-left: -44px;
    z-index: 1;
    width:140px;
    height:auto"  ></a>
        </li> 
        <li class="nav-item pt-2 ms-3 me-3 " >
          <a class="nav-link" style="padding-top: 50px; padding-left:120px" href="/engagements"  style="margin-left:70px;">Nos engagements</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3 ">
          <a class="nav-link" style="padding-top: 50px;" href="/faq">FAQ</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3">
          <a class="nav-link" style="padding-top: 50px;" href="/contact">Nous contacter</a>
        </li>
      </ul>
    </div>
  </nav>

  <!-- navigation bar ends here -->
  @yield('content')
  <div class="containerimage">
    <img class='imageTop' src="{{asset('/img/nosproduits.jpg')}}" alt="Cinque Terre" width="1000" height="300">
    <div class="topleftimagetxtnoborder text-white"><strong>
        <p class='h1 titrePage'>Nos produits</p>
      </strong></div>
  </div>

  <!-- bandeau noir top -->

  <!-- Photo + txt -->
  <div class='d-flex row pt-4 bg-secondary g-0'>
    <p class=' h2 text-center text-white'> <u>Nos offres</U></p>

    <div class='col-12 col-lg-4 border border-2 border-secondary'>
      <div class='d-flex row bg-dark g-0 pb-3'>
        <p class='text-center pb-2 pt-4 h3 text-white'>La Cellule </p>
        <div class="col-12 pb-3">
          <img class='pb-4 pe-3 ps-3' src="{{asset('/img/cellule.png')}}" alt="Cinque Terre" style="width: 100%; height:100%;">
        </div>
        <div class='col-12'>
          <p class='pb-4 text-center text-white  pe-2 ps-2'>La cellule est le « local » qui stockera le bois
            lors du cycle de séchage. La taille évolue en
            fonction du volume mensuel de bois séché. Par
            exemple pour 5m³ de bois la cellule fera 35m³
            et pour 150m³ de bois elle fera 800 m³. Les
            dimensions de ces cellules sont sur mesure et
            dépendent de la nature du bois séché (plots /

            avivées / carrelets).</p>
          <div class='text-center pb-5'>
            <button type="button" onclick="resetProduit1(),resetProduit2(),resetProduit3(),resetProduit1bis(),resetProduit2bis(),resetProduit3bis(),savoirPlus1(),savoirPlus1bis()" class="  bouttonStyle rounded btn btn-primary">Plus d'info</button>
          </div>
        </div>
      </div>
    </div>
    <div class='d-block d-lg-none d-xl-none' id="produit1bis" name="produit1"></div>


    <div class='col-12 col-lg-4 border border-2 border-secondary'>
      <div class='d-flex row bg-dark g-0 pb-3'>
        <p class='text-center pb-2 pt-4 h3 text-white'>L'unité de séchage </p>
        <div class="col-12 pb-3" >
          <img class='pe-3 ps-3' src="{{asset('/img/sechoir-ConvertImage.png')}}" alt="Cinque Terre" style="width: 100%; height:90%;">
        </div>
        <div class='col-12'>
          <p class='pb-2 text-center text-white  pe-2 ps-2' style="margin-top:-27px;">Le but est de produire de la
chaleur et de la canaliser dans
la cellule. En effet, nous devons
chauffer le bois et à la fois l’air
ambiant afin de permettre au
bois de sécher. Il faut
cependant contrôler les
paramètres de température et
d’humidité ambiante pour
obtenir un séchage optimal.
Nous devons lors du process
évaporer l’eau libre ainsi que
l’eau liée du bois par
évaporation. C’est une
machine thermodynamique</p>

          <div class='text-center pb-2'>
            <button type="button" onclick="resetProduit1(),resetProduit2(),resetProduit3(),resetProduit1bis(),resetProduit2bis(),resetProduit3bis(),savoirPlus2(),savoirPlus2bis()" class="  bouttonStyle rounded btn btn-primary">Plus d'info</button>
          </div>
        </div>
      </div>
    </div>
    <div class='d-block d-lg-none d-xl-none' id="produit2bis" name="produit2"></div>



    <div class='col-12 col-lg-4 border border-2 border-secondary'>
      <div class='d-flex row bg-dark g-0 pb-3'>
        <p class='text-center pb-2 pt-4 text-white  h3'>La régulation du séchage </p>
        <div class="col-12 pb-3">
          <img class='pb-4 pe-3 ps-3' src="{{asset('/img/regulation.png')}}" alt="Cinque Terre" style="width: 100%; height:98%;">
        </div>
        <div class='col-12'>
          <p class='pb-4 text-center text-white  pe-2 ps-2  'style="margin-top : 9px;">La régulation du séchoir sert à piloter et optimiser
            le cycle de séchage. Nous pouvons vous installer
            des appareils permettant de piloter le cycle de
            production et d’accéder aux informations en temps
            réels, jusqu’à l’enregistrement de données
            (notamment utilise pour le traitement NIMP15)</p>
          <div class='text-center pb-5' style="margin-bottom:9px;">
            <button type="button" onclick="resetProduit1(),resetProduit2(),resetProduit3(),resetProduit1bis(),resetProduit2bis(),resetProduit3bis(),savoirPlus3(),savoirPlus3bis()" class="  bouttonStyle rounded btn btn-primary">Plus d'info</button>
          </div>
        </div>
      </div>
    </div>
    <div class='d-none d-lg-block d-xl-block' id="produit1" name="produit1"></div>
    <div class='d-none d-lg-block d-xl-block' id="produit2" name="produit2"></div>
    <div class='d-none d-lg-block d-xl-block' id="produit3" name="produit3"></div>
    <div class='d-block d-lg-none d-xl-none' id="produit3bis" name="produit3bis"></div>
  </div>




  <footer class='pt-2 pb-2  mt-5 h-270px mediafooth' style="background: #452201">
      <div class="d-flex  rowmedia text-white">
        <div class="col-0 col-lg-2 ">
          <img class='ms-4 logonav d-none d-lg-block d-xl-block ' src="{{asset('/img/Logo industrie blanc png.png')}}">
        </div>
        <div class="col-6 col-lg-5 W-100percent">
          <div class="d-flex row mlfoot mt-3">
            <div class="col-12">
              <h1>Groupe wAys</h1>
            </div>
            <div class="col-12">
              <p>6 rue du Puits Bardin</p>
            </div>
            <div class="col-12">
              <p>77920 SAMOIS SUR SEINE</p>
            </div>
            <div class="col-12">
              <p>tél : 06.67.66.10.28</p>
            </div>
            <div class="col-12">
              <p>Mail : contact@w-ays.com</p>
            </div>
          </div>
        </div>
        <div class="col-0 col-lg-1 text-white">
          <span class=" d-none d-lg-block d-xl-block vertical-line"></span>
        </div>
        <div class="col-6 col-lg-4  displaynone mttopfootmedia W-100percent">
          <div class="d-flex  row">
            <div class="col-12 pt-5">
              <h1>Contactez nous</h1>
            </div>
            <div class="col-12 pt-5">
              <h1>Suivez nous aussi (logo)</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="d-flex text-white text-center displaynone2  row">
        <div class="col-6 pt-5">
          <h1>Contactez nous</h1>
        </div>
        <div class="col-6 pt-5">
          <h1>Suivez nous aussi (logo)</h1>
        </div>
      </div>
      <div class='d-flex text-white row'>
        <div class="ps-5 col-10">
          <a href="/legales" style="color: #8ebf42" >Mention légales</a>
        </div>
        <div class='col-2 '>
          <a href="/politique" style="color: #8ebf42" >Politique de confidentialité</a>
        </div>
      </div>

    </footer>
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="dist/app.js" type="text/javascript"></script>




</body>

</html>