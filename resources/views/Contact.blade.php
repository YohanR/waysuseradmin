<!doctype html>

<head>
    <title>@yield('title')</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Css additionel -->
    <link rel="stylesheet" href="css/app.css" />

    <!-- Bootstrap CSS -->
    <link href="dist/app.css" rel="stylesheet">

</head>

<body>

    <!-- navigation bar -->
    <!-- navigation bar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-bottom: 25px;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown"style="margin-bottom: 20px;">
      <ul class="navbar-nav">
        <li class="nav-item active pt-2 ms-3">
          <a class="nav-link" style="padding-top: 50px;" href="/">Accueil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px;" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Groupe wAys
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/quiSommesNous">Qui Sommes nous</a>
            <a class="dropdown-item" href="/technologie">Technologie</a>
            <a class="dropdown-item" href="/siteDeProduction">Site de Production</a>
            <a class="dropdown-item" href="/contact">Contact</a>
          </div>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px; padding-right:120px" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right:70px;">
            wAys Industrie
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/societeFiliale">Sociétés filiales</a>
            <a class="dropdown-item" href="/produits">Nos produits</a>
            <a class="dropdown-item" href="/maintenance">Maintenance</a>
            <a class="dropdown-item" href="/cahierDesCharges">Cahier des charges</a>
          </div>
        </li>
        <li class="nav-item ms-3 me-3 test2">
        <a class="nav-link d-none d-lg-block d-xl-block" href="/"><img class='logonav' src="{{asset('/img/Logo Ways Noir.png')}}" alt="Logo ways" style="position: absolute;
    margin-top: -17px;
    margin-left: -44px;
    z-index: 1;
    width:140px;
    height:auto"  ></a>
        </li> 
        <li class="nav-item pt-2 ms-3 me-3 " >
          <a class="nav-link" style="padding-top: 50px; padding-left:120px" href="/engagements"  style="margin-left:70px;">Nos engagements</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3 ">
          <a class="nav-link" style="padding-top: 50px;" href="/faq">FAQ</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3">
          <a class="nav-link" style="padding-top: 50px;" href="/contact">Nous contacter</a>
        </li>
      </ul>
    </div>
  </nav>

    <!-- navigation bar ends here -->
    @yield('content')
    <div class="d-flex ">
        <div class="col-6">
            <div class=" row align-items-center h-100">
                <div class=" col-12 ">
                    <div class="d-flex justify-content-center">
                        <div class="col-8 ">
                            <hr class='hrStyle'>
                            <span class='h1'><strong>Contactez-nous </strong></span>
                            <hr class='hrStyle'>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6 ">
            <div class="d-flex row justify-content-center">
                <img class='img-fluid w-75 pt-4 ' src="{{asset('/img/histoire-entreprise.jpg')}}" alt='des personnes avec un point dinterogation sur le visage'>
            </div>
        </div>
    </div>
    <form action="/contactAjout" method="GET">
        <div class='d-flex row pt-5'>
            <div class='col-1 col-lg-2'>
            </div>
            <div class='col-10 col-lg-3'>
                <div class="form-group">
                    <label for="exampleInputEmail1 ">Nom</label>
                    <input type="name" name='nom' class="form-control" id="nom" aria-describedby="emailHelp" placeholder="Entrer votre nom" required>
                </div>
                <div class="form-group pt-3">
                    <label for="exampleInputPassword1">Adresse mail</label>
                    <input type="email" name='mail' class="form-control" id="mail" placeholder="Entrer votre adresse mail" required>
                </div>
                <div class="form-group pt-3">
                    <label for="exampleInputPassword1">Entreprise</label>
                    <input type="text" name='entreprise' class="form-control" id="entreprise" placeholder="Nom de votre entreprise" required>
                </div>
            </div>
            <div class='d-none d-lg-block d-xl-bloc col-2 text-center'>
                <span class="vertical-line"></span>
            </div>
            <div class='col-11 col-lg-3 paddingformleft '>
                <div class="form-group">
                    <label for="exampleInputEmail1">Prénom</label>
                    <input type="name" name='prenom' class="form-control" id="prenom" aria-describedby="emailHelp" placeholder="Entrer votre prénom" required>
                </div>
                <div class="form-group pt-3">
                    <label for="exampleInputPassword1">Téléphone</label>
                    <input type="text" name='telephone' class="form-control" id="telephone" placeholder="Entrer votre numéro de téléphone" required>
                </div>
                <div class="form-group pt-3">
                    <label for="exampleInputPassword1">Code postale</label>
                    <input type="text" name='cp' class="form-control" id="cp" placeholder="Entrer votre code postale" required>
                </div>
            </div>
            <div class='col-0 col-lg-2'>
            </div>
        </div>
        <div class='text-center'>
            <div class='d-flex'>
                <div class='col-1 col-lg-2'>
                </div>
                <div class='col-10 col-lg-8'>
                    <div class="form-group pt-3">
                        <label for="exampleInputPassword1">Message</label>
                        <input type="Text" name='message' class="h-100px form-control" id="message" placeholder="Entrer votre Message">
                    </div>
                </div>
                <div class='col-1 col-lg-2'>
                </div>
            </div>
        </div>
        <div class="text-center form-check pt-3">
            <input type="checkbox" class=" float-none form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Je souhaite recevoir le cahier des charges et la brochures commerciale</label>
        </div>
        <div class='text-center pt-3 pb-3'>
            <button type="submit" class=" bouttonStyle rounded btn btn-primary">Submit</button>
        </div>
    </form>

    <!-- carte -->

        <div class="col-12 map-responsive">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22820.854370554916!2d2.552301903649968!3d44.35912810603954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b27d9845b9e425%3A0xaf5750dcd0f72c39!2sRodez!5e0!3m2!1sfr!2sfr!4v1623677519787!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
    </body>
    
    <footer class='pt-2 pb-2  mt-5  h-270px mediafooth' style="background: #452201">
      <div class="d-flex  rowmedia text-white">
        <div class="col-0 col-lg-2 ">
          <img class='ms-4 logonav d-none d-lg-block d-xl-block ' src="{{asset('/img/Logo industrie blanc png.png')}}">
        </div>
        <div class="col-6 col-lg-5 W-100percent">
          <div class="d-flex row mlfoot mt-3">
            <div class="col-12">
              <h1>Groupe wAys</h1>
            </div>
            <div class="col-12">
              <p>6 rue du Puits Bardin</p>
            </div>
            <div class="col-12">
              <p>77920 SAMOIS SUR SEINE</p>
            </div>
            <div class="col-12">
              <p>tél : 06.67.66.10.28</p>
            </div>
            <div class="col-12">
              <p>Mail : contact@w-ays.com</p>
            </div>
          </div>
        </div>
        <div class="col-0 col-lg-1 text-white">
          <span class=" d-none d-lg-block d-xl-block vertical-line"></span>
        </div>
        <div class="col-6 col-lg-4  displaynone mttopfootmedia W-100percent">
          <div class="d-flex  row">
            <div class="col-12 pt-5">
              <h1>Contactez nous</h1>
            </div>
            <div class="col-12 pt-5">
              <h1>Suivez nous aussi (logo)</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="d-flex text-white text-center displaynone2  row">
        <div class="col-6 pt-5">
          <h1>Contactez nous</h1>
        </div>
        <div class="col-6 pt-5">
          <h1>Suivez nous aussi (logo)</h1>
        </div>
      </div>
      <div class='d-flex text-white row pt-3 pb-3'>
        <div class="col-4">

        </div>
        <div class=" col-2">
          <a href="/legales" style="color: #8ebf42" >Mention légales</a>
        </div>
        <div class='col-2 '>
          <a href="/politique" style="color: #8ebf42" >Politique de confidentialité</a>
        </div>
        <div class="col-4">
          
          </div>
      </div>

    </footer>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="dist/app.js" type="text/javascript"></script>






</html>