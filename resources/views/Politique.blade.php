<!doctype html>

<head>
  <title>@yield('title')</title>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Css additionel -->
  <link rel="stylesheet" href="css/app.css" />

  <!-- Bootstrap CSS -->
  <link href="dist/app.css" rel="stylesheet">

</head>

<body>

  <!-- navigation bar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-bottom: 25px;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown"style="margin-bottom: 20px;">
      <ul class="navbar-nav">
        <li class="nav-item active pt-2 ms-3">
          <a class="nav-link" style="padding-top: 50px;" href="/">Accueil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px;" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Groupe wAys
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/quiSommesNous">Qui Sommes nous</a>
            <a class="dropdown-item" href="/technologie">Technologie</a>
            <a class="dropdown-item" href="/siteDeProduction">Site de Production</a>
            <a class="dropdown-item" href="/contact">Contact</a>
          </div>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px; padding-right:120px" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right:70px;">
            wAys Industrie
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/societeFiliale">Sociétés filiales</a>
            <a class="dropdown-item" href="/produits">Nos produits</a>
            <a class="dropdown-item" href="/maintenance">Maintenance</a>
            <a class="dropdown-item" href="/cahierDesCharges">Cahier des charges</a>
          </div>
        </li>
        <li class="nav-item ms-3 me-3 test2">
        <a class="nav-link d-none d-lg-block d-xl-block" href="/"><img class='logonav' src="{{asset('/img/Logo Ways Noir.png')}}" alt="Logo ways" style="position: absolute;
    margin-top: -17px;
    margin-left: -44px;
    z-index: 1;
    width:140px;
    height:auto"  ></a>
        </li> 
        <li class="nav-item pt-2 ms-3 me-3 " >
          <a class="nav-link" style="padding-top: 50px; padding-left:120px" href="/engagements"  style="margin-left:70px;">Nos engagements</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3 ">
          <a class="nav-link" style="padding-top: 50px;" href="/faq">FAQ</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3">
          <a class="nav-link" style="padding-top: 50px;" href="/contact">Nous contacter</a>
        </li>
      </ul>
    </div>
  </nav>

  <!-- navigation bar ends here -->
  @yield('content')
  <h1 class="text-center pt-3 pb-3" style="color: #8ebf42"><u><strong>Politique de confidentialité</strong></u></h1>

  <div class='d-flex row'>
    <div class="col-1 col-lg-2">
    </div>
    <div class='col-10 col-lg-8'>
      <h1  style="color: #87CEEB" >Utilisation des cookies</h1>
     <p> Groupe wAys utilise les cookies pour mesurer les audiences de son site internet,
afin de vous proposer un parcours visiteur optimisé et une expérience enrichie.</p>
<h1  style="color: #87CEEB">Qu&#39;est-ce qu&#39;un cookie ?</h1>
<p>Un cookie est un fichier texte, déposé et lu lors de la consultation d&#39;un site internet,
d&#39;une application mobile et ce, quel que soit le type de terminal utilisé (ordinateur,
smartphone, liseuse numérique, console de jeux vidéos connectée à Internet, etc.). <br> 
Nous utilisons sur ce site exclusivement des cookies d’analyse d&#39;audience nous
permettant de connaître le nombre de visiteurs sur notre site et de collecter des
informations sur la manière dont notre site est utilisé (par exemple, quels sont les
pages les plus vues par les visiteurs, quelles sont les types de recherches effectués,
etc.). Ces informations nous permettent d&#39;améliorer la qualité de notre site et
d&#39;enrichir votre expérience utilisateur. Les données issues de ces analyses ne sont
aucunement nominatives et sont conservées pour une durée de 26 mois. L&#39;utilisation
de ces données sont destinées au service Marketing.
L&#39;utilisation de ce cookie est soumise à votre consentement.</p>
<h1 style="color: #87CEEB">Que faire si vous ne souhaitez pas activer les cookies ?</h1>
<p>Vous pouvez refuser l’utilisation des cookies à tout moment :
En paramétrant les options de votre navigateur internet, option que l&#39;on trouvent
habituellement dans les menus « Outils », « Options », ou « Préférences ».
Voici ci-dessous une procédure pour désactiver les cookies dans les principaux
navigateurs :<br>
<a href="https://support.microsoft.com/fr-fr/windows/supprimer-et-g%C3%A9rer-les-cookies-168dab11-0753-043d-7c16-ede5947fc64d">Microsoft Internet Explorer</a><br>
<a href="https://support.mozilla.org/fr/kb/activer-desactiver-cookies-preferences?redirectlocale=fr&amp;redirectslug=activer-desactiver-cookies">Firefox</a><br>
<a href="https://support.google.com/accounts/answer/61416?hl=fr" >Google Chrome</a><br>
<a href="https://help.opera.com/en/latest/web-preferences/" >Opera</a><br>
<a href="https://support.apple.com/fr-fr/guide/safari/sfri11471/mac">Safari</a><br>
</P>
<h1 style="color: #87CEEB">Données personnelles - Contacter l&#39;entreprise</h1>
<p>Les données transmises dans le cadre d&#39;une demande de contact sont destinées aux
Services Marketing, Commercial ou Service Technique, en fonction de la nature de la
demande. La finalité du traitement des données transmises est de fournir une
réponse adéquate en fonction de la demande et de conserver un historique de la
demande initiale. Les données transmises sont conservées pour une durée d&#39;un an.
Les données peuvent faire l&#39;objet d&#39;une utilisation annexe dans le cadre de la relation
commerciale qui pourrait en découler, et sont destinées aux Services Marketing,
Commercial ou Service Client. Dans le cadre de cette utilisation annexe, les données
seront conservées pendant la durée strictement nécessaire à la gestion de la relation
commerciale. La demande a fait l&#39;objet de votre accord préalable. Toutefois,
conformément à la loi « informatique et libertés », vous pouvez exercer votre droit
d&#39;accès aux données vous concernant et les faire rectifier en nous contactant par
courrier électronique à l’adresse suivante : contact@w-ays.com.</p>

    </div>
    <div class="col-1 col-lg-2">
    </div>
  </div>

  <footer class='pt-2 pb-2  mt-5  h-270px mediafooth' style="background: #452201">
      <div class="d-flex  rowmedia text-white">
        <div class="col-0 col-lg-2 ">
          <img class='ms-4 logonav d-none d-lg-block d-xl-block ' src="{{asset('/img/Logo industrie blanc png.png')}}">
        </div>
        <div class="col-6 col-lg-5 W-100percent">
          <div class="d-flex row mlfoot mt-3">
            <div class="col-12">
              <h1>Groupe wAys</h1>
            </div>
            <div class="col-12">
              <p>6 rue du Puits Bardin</p>
            </div>
            <div class="col-12">
              <p>77920 SAMOIS SUR SEINE</p>
            </div>
            <div class="col-12">
              <p>tél : 06.67.66.10.28</p>
            </div>
            <div class="col-12">
              <p>Mail : contact@w-ays.com</p>
            </div>
          </div>
        </div>
        <div class="col-0 col-lg-1 text-white">
          <span class=" d-none d-lg-block d-xl-block vertical-line"></span>
        </div>
        <div class="col-6 col-lg-4  displaynone mttopfootmedia W-100percent">
          <div class="d-flex  row">
            <div class="col-12 pt-5">
              <h1>Contactez nous</h1>
            </div>
            <div class="col-12 pt-5">
              <h1>Suivez nous aussi (logo)</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="d-flex text-white text-center displaynone2  row">
        <div class="col-6 pt-5">
          <h1>Contactez nous</h1>
        </div>
        <div class="col-6 pt-5">
          <h1>Suivez nous aussi (logo)</h1>
        </div>
      </div>
      <div class='d-flex text-white row pt-3 pb-3'>
        <div class="col-4">

        </div>
        <div class=" col-2">
          <a href="/legales" style="color: #8ebf42" >Mention légales</a>
        </div>
        <div class='col-2 '>
          <a href="/politique" style="color: #8ebf42" >Politique de confidentialité</a>
        </div>
        <div class="col-4">
          
          </div>
      </div>

    </footer>





  <!-- jQuery first, then Popper.js, then Bootstrap JS -->

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="dist/app.js" type="text/javascript"></script>




</body>

</html>