<!doctype html>

<head>
    <title>@yield('title')</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Css additionel -->
    <link rel="stylesheet" href="css/app.css" />

    <!-- Bootstrap CSS -->
    <link href="dist/app.css" rel="stylesheet">

</head>

<body>

    <!-- navigation bar -->
<!-- navigation bar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-bottom: 25px;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown"style="margin-bottom: 20px;">
      <ul class="navbar-nav">
        <li class="nav-item active pt-2 ms-3">
          <a class="nav-link" style="padding-top: 50px;" href="/">Accueil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px;" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Groupe wAys
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/quiSommesNous">Qui Sommes nous</a>
            <a class="dropdown-item" href="/technologie">Technologie</a>
            <a class="dropdown-item" href="/siteDeProduction">Site de Production</a>
            <a class="dropdown-item" href="/contact">Contact</a>
          </div>
        </li>
        <li class="nav-item dropdown pt-2 ms-3 me-3 ">
          <a class="nav-link dropdown-toggle" style="padding-top: 50px; padding-right:120px" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right:70px;">
            wAys Industrie
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/societeFiliale">Sociétés filiales</a>
            <a class="dropdown-item" href="/produits">Nos produits</a>
            <a class="dropdown-item" href="/maintenance">Maintenance</a>
            <a class="dropdown-item" href="/cahierDesCharges">Cahier des charges</a>
          </div>
        </li>
        <li class="nav-item ms-3 me-3 test2">
        <a class="nav-link d-none d-lg-block d-xl-block" href="/"><img class='logonav' src="{{asset('/img/Logo Ways Noir.png')}}" alt="Logo ways" style="position: absolute;
    margin-top: -17px;
    margin-left: -44px;
    z-index: 1;
    width:140px;
    height:auto"  ></a>
        </li> 
        <li class="nav-item pt-2 ms-3 me-3 " >
          <a class="nav-link" style="padding-top: 50px; padding-left:120px" href="/engagements"  style="margin-left:70px;">Nos engagements</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3 ">
          <a class="nav-link" style="padding-top: 50px;" href="/faq">FAQ</a>
        </li>
        <li class="nav-item pt-2 ms-3 me-3">
          <a class="nav-link" style="padding-top: 50px;" href="/contact">Nous contacter</a>
        </li>
      </ul>
    </div>
  </nav>

    <!-- navigation bar ends here -->
    @yield('content')


    <div class="containerimage">
        <img class='imageTop' src="{{asset('/img/filiale.jpeg')}}" alt="Cinque Terre" width="1000" height="300">
        <div class="toprightimagetxtborder text-white"><strong>
                <p class='h1 titrePage'>Société<br> filiales</p>
            </strong></div>
    </div>

    <!--text-->
    <div class='d-flex  pt-5 positionRelative mb-3'>
        <div class='col-12 col-lg-8 bg-secondary'>
            <div class='d-flex  row  pt-2 pb-2'>
                <div class='col-12'>
                    <p class='h2 text-white ps-3'>Trouver titre</p>
                </div>
                <div class='col-12 align-item'>
                    <p class='text-white ps-3'>wAys INDUSTRIE est une société filiale de wAys. Elle conçoit, fabrique, installe et maintien des
séchoirs à bois. Elle étudie les besoins clients et propose des solutions sur mesure. Le
dimensionnement et l’étude de vos demandes sont réalisés en interne. Nous nous efforçons de
répondre à vos besoins tout en prenant compte des innovations technologique sur le marché et
écologique. Nous proposons principalement des séchoirs par déshumidification, mais nous étudions
chaque demande. Notre filiale est l’antenne de production de l’innovation technologique en matière
de séchage porté par le groupe wAys.
                    </p>
                </div>
            </div>
        </div>
        <div class='col-0 col-lg-4' height="100%">
            <img class='d-none d-lg-block d-xl-block  ' src="{{asset('/img/bancheprixel.jpg')}}" alt="Cinque Terre" height="100%" width="100%">
        </div>
    </div>

    <!--Design-->

    <div class='ms-4 text-center '>
<a href="/produits">
<img class='cartemediafiliale'  src="{{asset('/img/Carte_france.png')}}" alt="Cinque Terre" height="auto" width="40%">
</a>
</div>

   

    <!--text-->
    <div class='d-flex row pt-5 positionRelative mb-3'>
        <div class='col-12 col-lg-4 ps-0 pe-0'>
            <img class='d-none d-lg-block d-xl-block  ' src="{{asset('/img/bancheprixel.jpg')}}" alt="Cinque Terre" height="100%" width="100%">
        </div>
        <div class='col-0 col-lg-8   bg-secondary' height="100%">
            <div class='d-flex row  pt-2 pb-2'>
                <div class='col-12'>
                    <p class='h2 text-white ps-3'>Trouver titre  (1)</p>
                </div>
                <div class='col-12'>
                    <p class='text-white ps-3'>Notre société est une PME qui garantit un savoir faire artisanal avec une gestion industrielle. Notre
Directeur Général, Guillaume CARMASSI est un ingénieur de l’école des Mines d’Albi spécialisé en
génie industriel. Fort d’une dizaine d’année d’expériences dans le milieu industriel du bois (surtout
seconde transformation), il a pris en mai 2021 la direction de notre filiale. Fort de son cursus
d’ingénieur (formation Qualibat en pompe à chaleur obtenu), nous réalisons tous les calculs de
déperditions et de besoins en chaleurs en interne. Le principe fort que nous souhaitons mettre en
avant dans notre société est sécher du bois de façon pérenne et à moindre coût énergétique. Une
des solutions les plus adaptés est la pompe à chaleur. Elle permet si les calculs sont bien réalisés pour
1KW électrique consommé nous pouvons aller jusqu’à 3 KW de chaleur. Pour les autres principes
existants sur le marché c’est du 1 pour 1.</p>
                </div>
            </div>
        </div>
    </div>


    <!--Design-->


    <footer class='pt-2 pb-2  mt-5  h-270px mediafooth' style="background: #452201">
      <div class="d-flex  rowmedia text-white">
        <div class="col-0 col-lg-2 ">
          <img class='ms-4 logonav d-none d-lg-block d-xl-block ' src="{{asset('/img/Logo industrie blanc png.png')}}">
        </div>
        <div class="col-6 col-lg-5 W-100percent">
          <div class="d-flex row mlfoot mt-3">
            <div class="col-12">
              <h1>Groupe wAys</h1>
            </div>
            <div class="col-12">
              <p>6 rue du Puits Bardin</p>
            </div>
            <div class="col-12">
              <p>77920 SAMOIS SUR SEINE</p>
            </div>
            <div class="col-12">
              <p>tél : 06.67.66.10.28</p>
            </div>
            <div class="col-12">
              <p>Mail : contact@w-ays.com</p>
            </div>
          </div>
        </div>
        <div class="col-0 col-lg-1 text-white">
          <span class=" d-none d-lg-block d-xl-block vertical-line"></span>
        </div>
        <div class="col-6 col-lg-4  displaynone mttopfootmedia W-100percent">
          <div class="d-flex  row">
            <div class="col-12 pt-5">
              <h1>Contactez nous</h1>
            </div>
            <div class="col-12 pt-5">
              <h1>Suivez nous aussi (logo)</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="d-flex text-white text-center displaynone2  row">
        <div class="col-6 pt-5">
          <h1>Contactez nous</h1>
        </div>
        <div class="col-6 pt-5">
          <h1>Suivez nous aussi (logo)</h1>
        </div>
      </div>
      <div class='d-flex text-white row pt-3 pb-3'>
        <div class="col-4">

        </div>
        <div class=" col-2">
          <a href="/legales" style="color: #8ebf42" >Mention légales</a>
        </div>
        <div class='col-2 '>
          <a href="/politique" style="color: #8ebf42" >Politique de confidentialité</a>
        </div>
        <div class="col-4">
          
          </div>
      </div>

    </footer>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="dist/app.js" type="text/javascript"></script>




</body>

</html>